// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // api_base_url: 'https://hamuy.raco.dev/api',
  // image_base_url: 'https://hamuy.raco.dev',
  //api_base_url: 'https://dokoit.com/doko/hamuy/public/api',
  //image_base_url: 'https://dokoit.com/doko/hamuy/storage',
  api_base_url: 'https://hamuyperu.com/public/api',
  image_base_url: 'https://hamuyperu.com/storage',
  version: '1.0.0',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
