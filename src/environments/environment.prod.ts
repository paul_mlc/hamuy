export const environment = {
  production: true,
  api_base_url: 'https://hamuyperu.com/public/api',
  image_base_url: 'https://hamuyperu.com/storage',
  version: '1.0.0',
};
