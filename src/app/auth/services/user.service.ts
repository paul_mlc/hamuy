import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpService: HttpService) { }

  public signIn(user) {
    console.log('el user es ', user);
    return this.httpService.post(environment.api_base_url + '/user/login', user);
  }

  public signUp(user) {
    console.log('el user es ', user);
    // let temp = this.httpService.post(environment.api_base_url + '/user/store', user);
    // return false;
    return this.httpService.post(environment.api_base_url + '/user/store', user);
  }

  public sendSMS(phone) {
    return this.httpService.post(environment.api_base_url + '/user/sendsms', phone);
  }

  public verify(verifyDto) {
    return this.httpService.post(environment.api_base_url + '/user/verify', verifyDto);
  }

  public addresses() {
    return this.httpService.get(environment.api_base_url + '/addressList');
  }

  public deleteAddress(addressId) {
    return this.httpService.post(environment.api_base_url + '/address/' + addressId + '/delete');
  }

  public profile() {
    return this.httpService.get(environment.api_base_url + '/user/profile');
  }

  public updateProfile(data) {
    return this.httpService.put(environment.api_base_url + '/user/profile', data);
  }

  public createAddress(address) {
    return this.httpService.post(environment.api_base_url + '/address/store', address);
  }

  public paymentMethods() {
    return this.httpService.get(environment.api_base_url + '/paymentMethodsList');
  }

  public orders(): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/orders');
  }

  // public ordersm(): Observable<any> {
  //   return this.httpService.get(environment.api_base_url + '/ordersm');
  // }

  public uploadAvatar(request): Observable<any> {
    return this.httpService.post(environment.api_base_url + '/user/avatar', request);
  }

  public favourites(): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/favourites');
  }

  public addOrRemoveFavourite(request): Observable<any> {
    return this.httpService.post(environment.api_base_url + '/favourites/addOrRemove', request);
  }





}
