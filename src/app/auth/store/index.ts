import { createSelector, createFeatureSelector } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { Auth as AuthState } from '../auth.interface';
// export const selectAuth = (state: State) => state.auth;
export const getAuthState = createFeatureSelector<State, AuthState>('auth');

export const selectGetAuth = createSelector(
    getAuthState,
    (state: AuthState) => state
);

export const selectGetUser = createSelector(
    getAuthState,
    (state: AuthState) => state.user
);

export const selectIsLoggedIn = createSelector(
    getAuthState,
    (state: AuthState) => !!Object.keys(state.user).length
);

export const selectAddresses = createSelector(
    getAuthState,
    (state: AuthState) => state.addresses
);

export const selectCurrentAddress = createSelector(
    getAuthState,
    (state: AuthState) => state.currentAddress
);

export const selectPaymentMethdos = createSelector(
    getAuthState,
    (state: AuthState) => state.paymentMethods
);

export const selectFavourites = createSelector(
    getAuthState,
    (state: AuthState) => state.favourites
);

export const selectCurrentCard = createSelector(
    getAuthState,
    (state: AuthState) => state.currentCard
);

export const selectModalidad = createSelector(
    getAuthState,
    (state: AuthState) => state.modalidad
);




