import { createAction, props } from '@ngrx/store';

export enum AuthActionTypes {
    ClearAuth = '[Auth Page] Clear Auth',
    ClearSliders = '[Auth Page] Clear Sliders',
    SignIn = '[Auth Page] Sign In',
    SignInSuccess = '[Auth API] Sign In Success',
    SignInFailure = '[Auth API] Sign In Failure',
    SignInWithPhone = '[Auth Page] Sign In With Phone',
    Verify = '[Auth Page] Verify',
    VerifySuccess = '[Auth API] Verify Success',
    VerifyFailure = '[Auth API] Verify Failure',
    SignUp = '[Auth Page] Sign Up',
    StoreFirebaseToken = '[Firebase] Store token',
    UpdateUser = '[User] Update user',
    InitUserData = '[User] Init User Data',
    InitUserDataSuccess = '[User] Init User Data Success',
    InitUserDataFailure = '[User] Init User Data Failure',
    GetPaymentMethods = '[User] Get Payment Methods',
    GetPaymentMethodsSuccess = '[User] Get Payment Methods Success',
    GetPaymentMethodsFailure = '[User] Get Payment Methods Failure',
    GetAddresses = '[User] Get Addresses',
    GetAddressesSuccess = '[User] Get Addresses Success',
    GetAddressesFailure = '[User] Get Addresses Failure',
    GetOrders = '[User] Get Orders',
    GetOrdersSuccess = '[User] Get Orders Success',
    GetOrdersFailure = '[User] Get Orders Failure',
    UpdatePositionSuccess = '[Employee] Update Position Success',
    SetCurrentAddress = '[User] Set Current Address',
    AddAddress = '[User] Add Address',
    AddAddressSuccess = '[User] Add Address Success',
    AddAddressFailure = '[User] Add Address Failure',
    DeleteAddress = '[User] Delete Address',
    GetFavourites = '[User] Get Favourites',
    GetFavouritesSuccess = '[User] Get Favourites Success',
    GetFavouritesFailure = '[User] Get Favourites Failure',
    AddFavourite = '[User] Add Favourite',
    AddFavouriteSuccess = '[User] Add Favourite Success',
    AddFavouriteFailure = '[User] Add Favourite Failure',
    SetCurrentCard = '[User] Set Current Card',
    InitCurrentCard = '[User] Init Current Card',
}

export const clearAuth = createAction(
    AuthActionTypes.ClearAuth
);

export const clearSliders = createAction(
    AuthActionTypes.ClearSliders
);

export const signIn = createAction(
    AuthActionTypes.SignIn,
    props<{ email: string; password: string }>()
);
export const signInSuccess = createAction(
    AuthActionTypes.SignInSuccess,
    props<any>()
);
export const signInFailure = createAction(
    AuthActionTypes.SignInFailure,
    props<any>()
);

export const signInWithPhone = createAction(
    AuthActionTypes.SignInWithPhone,
    props<any>()
);

export const verify = createAction(
    AuthActionTypes.Verify,
    props<any>()
);
export const verifySuccess = createAction(
    AuthActionTypes.VerifySuccess,
    props<any>()
);
export const verifyFailure = createAction(
    AuthActionTypes.VerifyFailure,
    props<any>()
);

export const signUp = createAction(
    AuthActionTypes.SignUp,
    props<{ email: string; password: string }>()
);

export const initUserData = createAction(
    AuthActionTypes.InitUserData
);
export const initUserDataSuccess = createAction(
    AuthActionTypes.InitUserDataSuccess,
    props<any>()
);
export const initUserDataFailure = createAction(
    AuthActionTypes.InitUserDataFailure,
    props<any>()
);

export const getAddresses = createAction(
    AuthActionTypes.GetAddresses
);
export const getAddressesSuccess = createAction(
    AuthActionTypes.GetAddressesSuccess,
    props<any>()
);
export const getAddressesFailure = createAction(
    AuthActionTypes.GetAddressesFailure,
    props<any>()
);

export const getFavourites = createAction(
    AuthActionTypes.GetFavourites
);
export const getFavouritesSuccess = createAction(
    AuthActionTypes.GetFavouritesSuccess,
    props<any>()
);
export const getFavouritesFailure = createAction(
    AuthActionTypes.GetFavouritesFailure,
    props<any>()
);

export const getPaymentMethods = createAction(
    AuthActionTypes.GetPaymentMethods
);
export const getPaymentMethodsSuccess = createAction(
    AuthActionTypes.GetPaymentMethodsSuccess,
    props<any>()
);
export const getPaymentMethodsFailure = createAction(
    AuthActionTypes.GetPaymentMethodsFailure,
    props<any>()
);

export const getOrders = createAction(
    AuthActionTypes.GetOrders
);
export const getOrdersSuccess = createAction(
    AuthActionTypes.GetOrdersSuccess,
    props<any>()
);
export const getOrdersFailure = createAction(
    AuthActionTypes.GetOrdersFailure,
    props<any>()
);

export const storeFirebaseToken = createAction(
    AuthActionTypes.StoreFirebaseToken,
    props<{ fbtoken: string; }>()
);

export const updateUser = createAction(
    AuthActionTypes.UpdateUser,
    props<any>()
);

export const setCurrentAddress = createAction(
    AuthActionTypes.SetCurrentAddress,
    props<any>()
);

export const addAddress = createAction(
    AuthActionTypes.AddAddress,
    props<any>()
);
export const addAddressSuccess = createAction(
    AuthActionTypes.AddAddressSuccess,
    props<any>()
);
export const addAddressFailure = createAction(
    AuthActionTypes.AddAddressFailure,
    props<any>()
);
export const deleteAddress = createAction(
    AuthActionTypes.DeleteAddress,
    props<any>()
);

export const addFavourite = createAction(
    AuthActionTypes.AddFavourite,
    props<any>()
);
export const addFavouriteSuccess = createAction(
    AuthActionTypes.AddFavouriteSuccess,
    props<any>()
);
export const addFavouriteFailure = createAction(
    AuthActionTypes.AddFavouriteFailure,
    props<any>()
);

export const setCurrentCard = createAction(
    AuthActionTypes.SetCurrentCard,
    props<any>()
);

export const initCurrentCard = createAction(
    AuthActionTypes.InitCurrentCard,
    props<any>()
);
