import * as AuthActions from './auth.actions';
import { Auth as AuthState } from '../auth.interface';
import { on, Action, createReducer } from '@ngrx/store';

export const initialState: AuthState = {
    error: null,
    user: {},
    addresses: [],
    currentAddress: null,
    paymentMethods: null,
    currentCard: null,
    fbtoken: '',
    showSliders: true,
    favourites: [],
    modalidad: 0,
    access_token: ''
};

const authReducer = createReducer(
    initialState,
    on(AuthActions.clearAuth, () => ({ ...initialState, showSliders: false })),
    on(AuthActions.clearSliders, (state) => ({ ...state, showSliders: false })),
    on(AuthActions.storeFirebaseToken, (state, payload) => ({ ...state, fbtoken: payload.fbtoken })),
    on(AuthActions.signInSuccess, (state, payload) => ({ ...state, ...payload })),
    on(AuthActions.updateUser, (state, payload) => ({ ...state, user: payload })),
    on(AuthActions.signInFailure, (state, payload) => ({ ...state, error: payload.error.error })),
    on(AuthActions.getAddressesSuccess, (state, payload) => ({ ...state, addresses: payload.addressList })),
    on(AuthActions.getPaymentMethodsSuccess, (state, payload) => ({ ...state, paymentMethods: payload.data })),
    on(AuthActions.setCurrentAddress, (state, payload) => ({ ...state, currentAddress: payload.address })),
    on(AuthActions.addAddressSuccess, (state, payload) => {
        const addresses = [...(state.addresses || [])];
        return ({ ...state, addresses: [...addresses, payload.address], currentAddress: payload.address });
    }),
    on(AuthActions.signInWithPhone, (state) => ({ ...state, error: '' })),
    on(AuthActions.verifySuccess, (state, payload) => {
        const user = { ...state.user };
        return ({ ...state, user: { ...user, verified: true } });
    }),
    on(AuthActions.verifyFailure, (state, payload) => ({ ...state, error: payload.error.error })),
    on(AuthActions.getFavouritesSuccess, (state, payload) => ({ ...state, favourites: payload.favourites })),
    on(AuthActions.setCurrentCard, (state, payload) => ({ ...state, currentCard: payload.currentCard })),
    on(AuthActions.initCurrentCard, (state, payload) => {
        const currentCard = { ...state.currentCard };
        return ({ ...state, currentCard: { ...currentCard, payment_token: '' } });
    }),
);

export function reducer(state: AuthState, action: Action) {
    return authReducer(state, action);
}
