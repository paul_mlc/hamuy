import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of, throwError, forkJoin } from 'rxjs';
import { catchError, exhaustMap, map, tap, switchMap, concatMap } from 'rxjs/operators';

import * as AuthActions from './auth.actions';

import { LoadingController } from '@ionic/angular';
import { UserService } from '../services/user.service';

@Injectable()
export class AuthEffects {
    loading: any;
    
    signIn$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.AuthActionTypes.SignIn),
            tap(this.presentLoading.bind(this)),
            exhaustMap(payload =>
                this.userService.signIn(payload).pipe(
                    map(user => AuthActions.signInSuccess({ ...user })),
                    catchError(error => of(AuthActions.signInFailure({ error })))
                )
            ),
            tap(this.dissmisLoading.bind(this)),
        )
    );

    verify$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.AuthActionTypes.Verify),
            tap(this.presentLoading.bind(this)),
            exhaustMap(payload =>
                this.userService.verify(payload).pipe(
                    map(user => AuthActions.signInSuccess({ ...user })),
                    catchError(error => of(AuthActions.verifyFailure({ error} )))
                )
            ),
            tap(this.dissmisLoading.bind(this)),
        )
    );

    initUserData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.AuthActionTypes.InitUserData),
            switchMap(() => {
                console.log('FORK JOIN!!!');
                return forkJoin([
                    this.userService.addresses(),
                    this.userService.paymentMethods()
                ]);
            }
            ),
            concatMap((r: any) => {
                console.log('CONCAT MAP');

                return [
                    AuthActions.getAddressesSuccess(r[0]),
                    AuthActions.getPaymentMethodsSuccess(r[1]),
                ];
            }),
            catchError(error => of(AuthActions.initUserDataFailure({ error })))
        )
    );

    signUp$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.AuthActionTypes.SignUp),
            tap(this.presentLoading.bind(this)),
            exhaustMap(payload =>
                this.userService.signUp(payload).pipe(
                    map(user => AuthActions.signInSuccess({ ...user })),
                    catchError(error => of(AuthActions.verifyFailure(error)))
                )
            ),
            tap(this.dissmisLoading.bind(this)),
        ),
        { useEffectsErrorHandler: false }
    );

    addAddress$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.AuthActionTypes.AddAddress),
            tap(this.presentLoading.bind(this)),
            exhaustMap(payload =>
                this.userService.createAddress(payload).pipe(
                    map(res => AuthActions.addAddressSuccess({ address: res.data })),
                    catchError(error => of(AuthActions.signInFailure(error)))
                )
            ),
            tap(this.dissmisLoading.bind(this))
        )
    );

    deleteAddress$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.AuthActionTypes.DeleteAddress),
            exhaustMap((payload: any) =>
                this.userService.deleteAddress(payload.address.id).pipe(
                    map(data => AuthActions.initUserData()),
                    catchError(error => of(AuthActions.signInFailure(error)))
                )
            )
        ),
        { useEffectsErrorHandler: false }
    );

    getFavourites$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.AuthActionTypes.GetFavourites),
            exhaustMap((payload: any) =>
                this.userService.favourites().pipe(
                    map(res => AuthActions.getFavouritesSuccess({ favourites: res.favourites })),
                    catchError(error => of(AuthActions.getFavouritesFailure(error)))
                )
            )
        ),
        { useEffectsErrorHandler: false }
    );

    addFavourite$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.AuthActionTypes.AddFavourite),
            tap(this.presentLoading.bind(this)),
            exhaustMap(payload =>
                this.userService.addOrRemoveFavourite(payload).pipe(
                    map(res => AuthActions.getFavouritesSuccess({ favourites: res.favourites })),
                    catchError(error => of(AuthActions.addFavouriteFailure(error)))
                )
            ),
            tap(this.dissmisLoading.bind(this))
        )
    );




    constructor(
        private actions$: Actions,
        private userService: UserService
    ) { }

    async presentLoading() {
        this.loading = await (new LoadingController()).create({
            message: '<ion-img src="/assets/images/logo.png" alt="loading..."></ion-img>',
            cssClass: 'default-loading',
            translucent: true,
            showBackdrop: true,
            spinner: null,
        });
        await this.loading.present();
    }

    dissmisLoading() {
        //if(this.loading){
            // try {
                this.loading.dismiss();
            // } catch(e) {
            //     console.log(e); 
            // }
            
        //}
    }
}
