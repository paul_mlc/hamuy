export interface Auth {
    access_token?: any;
    user?: any;
    addresses?: any;
    currentAddress?: any;
    paymentMethods?: any;
    currentCard?: any;
    error?: any;
    fbtoken?: string;
    showSliders?: boolean;
    favourites?: any;
    modalidad?: any;
}
