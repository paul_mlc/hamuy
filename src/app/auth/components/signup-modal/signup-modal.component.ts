import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
    selector: 'signup-modal-page',
    templateUrl: './signup-modal.component.html',
    styleUrls: ['./signup-modal.component.scss'],
})
export class SignUpModalPage {
    constructor(
        private router: Router,
        private modalCtrl: ModalController) {
    }

    dismiss() {
        this.modalCtrl.dismiss({
            dismissed: true
        });
    }

    goToLogin() {
        this.dismiss();
        this.router.navigate(['auth/signin']);
    }
}
