import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { ProfilePage } from './pages/profile/profile.page';
import { SharedModule } from '../shared/shared.module';
import { InvoicingPage } from './pages/invoicing/invoicing.page';
import { TycPage } from './pages/tyc/tyc.page';
import { FaqPage } from './pages/faq/faq.page';
import { ConfigPage } from './pages/config/config.page';
import { OrdersPage } from './pages/orders/orders.page';
import { SignUpModalPage } from './components/signup-modal/signup-modal.component';
import { AuthEffects } from './store/auth.effects';
import { EffectsModule } from '@ngrx/effects';
import { AddressesPage } from './pages/addresses/addresses.page';
import { IntroPage } from './pages/intro/intro.page';
import { SignupPage } from './pages/signup/signup.page';
import { SigninPage } from './pages/signin/signin.page';
import { PhonePage } from './pages/phone/phone.page';
import { VerifyPage } from './pages/verify/verify.page';
import { UserPage } from './pages/user/user.page';
import { FavouritesPage } from './pages/favourites/favourites.page';

const routes: Routes = [
  {
    path: '',
    component: IntroPage
  },
  {
    path: 'signup',
    component: SignupPage
  },
  {
    path: 'signin',
    component: SigninPage
  },
  {
    path: 'phone',
    component: PhonePage
  },
  {
    path: 'verify',
    component: VerifyPage
  },
  {
    path: 'user',
    component: UserPage
  },
  { path: 'addresses', component: AddressesPage },
  { path: 'favourites', component: FavouritesPage },
  { path: 'profile',component: ProfilePage},
  { path: 'invoicing', component: InvoicingPage },
  { path: 'tyc', component: TycPage },
  { path: 'faq', component: FaqPage },
  { path: 'config', component: ConfigPage },
  { path: 'orders', component: OrdersPage },
];

@NgModule({
  entryComponents: [
    SignUpModalPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
    EffectsModule.forFeature([AuthEffects])
  ],
  declarations: [
    IntroPage,
    SignupPage,
    SigninPage,
    PhonePage,
    UserPage,
    ProfilePage,
    InvoicingPage,
    TycPage,
    FaqPage,
    ConfigPage,
    OrdersPage,
    SignUpModalPage,
    AddressesPage,
    VerifyPage,
    FavouritesPage
  ],
  providers: [
  ]
})
export class AuthModule { }
