import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, IonSlides } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { clearSliders } from '../../store/auth.actions';
import { getAuthState } from '../../store';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  // @ViewChild('slider', { static: true }) slides: IonSlides;

  showSlides = true;

  constructor(
    private router: Router,
    private store: Store<State>
  ) {
    console.log('IntroPage');
  }

  ngOnInit() {
    this.store.select(getAuthState).subscribe(auth => {
      this.showSlides = auth.showSliders;
    });
  }

  swipeNext(slider) {
    console.log('SLIDE!');
    slider.slideNext();
  }

  clearSliders() {
    this.store.dispatch(clearSliders());
  }

  goToSignUp() {
    this.router.navigate(['auth/signup']);
  }

  goToSignIn() {
    this.router.navigate(['auth/signin']);
  }

  goToSignInWithPhone() {
    this.router.navigate(['auth/phone']);
  }
}
