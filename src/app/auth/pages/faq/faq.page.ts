import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  openCategory() {
    this.router.navigateByUrl('services/category');
  }
}
