import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
import { UserService } from 'src/app/auth/services/user.service';

@Injectable({
    providedIn: 'root'
})
export class ProfileResolverService implements Resolve<any> {
    constructor(private userService: UserService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return this.userService.profile().pipe(map(r => r.data));
    }
}
