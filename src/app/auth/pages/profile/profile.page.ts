import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/auth/services/user.service';
import { ToastController, NavController } from '@ionic/angular';
import { USER } from 'src/app/shared/constants/global.constants';
import { DateTimeHelper } from 'src/app/shared/helpers/datetime.helper';
import { selectGetUser } from '../../store';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../../delivery/store';
import { selectModalidad } from 'src/app/auth/store';
import { updateUser } from '../../store/auth.actions';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  profileForm;
  user: any;
  modalidad: number;
  cartLength:number=0;
  
  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private toastController: ToastController,
    private store: Store<State>,
    private navCtrl: NavController,
  ) {
    // this.activatedRoute.data.subscribe(data => {
    //   if (data.profile) {
    //     this.profileForm = this.createProfileForm(data.profile);
    //   }
    // });
    this.store.pipe(select(selectGetUser)).subscribe(user => this.user = user);
    this.profileForm = this.createProfileForm();
  }

  ngOnInit() {
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });

  }

  ionViewDidEnter() {
    this.store.select(selectModalidad).subscribe(modalidad => {
      this.modalidad = modalidad;
    });
  }


  createProfileForm() {
    return this.fb.group({
      name: [this.user.name, [Validators.required]],
      last_name: [this.user.last_name, [Validators.required]],
      phone: [this.user.phone, [Validators.required, Validators.pattern('[0-9]{9}')]],
      // birthdate: [''],
      password: [],
      confirmPassword: []
    },
      { validator: this.checkPassword('password', 'confirmPassword') }
    );
  }
  // createProfileForm(profile) {
  //   return this.fb.group({
  //     name: [profile.name, [Validators.required]],
  //     last_name: [profile.last_name, [Validators.required]],
  //     phone: [profile.phone, [Validators.required, Validators.pattern('[0-9]{9}')]],
  //     birthdate: [profile.birthdate],
  //     password: [],
  //     confirmPassword: []
  //   },
  //     { validator: this.checkPassword('password', 'confirmPassword') }
  //   );
  // }

  onSubmit() {
    const profile = Object.assign({}, this.profileForm.value);
    profile.birthdate = DateTimeHelper.formatToDDMMYYYY(new Date(profile.birthdate));
    this.userService.updateProfile(profile)
      .subscribe(res => {
        this.store.dispatch(updateUser(res.data));
        this.presentSuccessToast();
      });
    this.navCtrl.back();
  }

  async presentSuccessToast() {
    const toast = await this.toastController.create({
      message: USER.profile.update.success,
      duration: 4000,
      color: 'success',
      position: 'top'
    });
    toast.present();
  }

  checkPassword(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }
      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

}
