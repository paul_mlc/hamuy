import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { State } from 'src/app/app.reducer';
import { Store, select } from '@ngrx/store';
import { selectAddresses, selectCurrentAddress } from '../../store';
import { NewAddressPage } from 'src/app/shared/components/new-address/new-address.component';
import { ModalController, ActionSheetController, NavController } from '@ionic/angular';
import { setCurrentAddress, deleteAddress } from '../../store/auth.actions';

@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.page.html',
  styleUrls: ['./addresses.page.scss'],
})
export class AddressesPage implements OnInit {

  addresses$;
  address$;

  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private navCtrl: NavController
  ) {
    this.addresses$ = this.store.pipe(select(selectAddresses));
    this.address$ = this.store.pipe(select(selectCurrentAddress));
  }

  ngOnInit() {
  }

  async openNewAddressModal() {
    const modal = await this.modalController.create({
      component: NewAddressPage,
    });
    modal.onDidDismiss().then(() => {
    });
    return await modal.present();
  }

  async openOptions(address) {
    const actionSheet = await this.actionSheetController.create({
      header: address.address,
      buttons: [{
        text: 'Seleccionar como actual',
        role: 'destructive',
        icon: 'star',
        handler: () => {
          this.store.dispatch(setCurrentAddress({ address }));
          this.navCtrl.back();
        }
      }, {
        text: 'Eliminar dirección',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log(address);
          this.store.dispatch(deleteAddress({ address }));
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
}
