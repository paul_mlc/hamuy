import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { clearAuth, setCurrentAddress, deleteAddress } from '../../store/auth.actions';
import { State } from 'src/app/app.reducer';
import { Store, select } from '@ngrx/store';
import { cartLength } from '../../../delivery/store';
import { ModalController, ActionSheetController, NavController } from '@ionic/angular';
import { selectAddresses, selectCurrentAddress, selectGetUser, selectModalidad } from '../../store';
import { NewAddressPage } from 'src/app/shared/components/new-address/new-address.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  addresses: any = [];
  address: any;
  user: any;
  modalidad: number;
  cartLength:number=0;
  
  constructor(
    private router: Router,
    private actionSheetController: ActionSheetController,
    private navCtrl: NavController,
    private modalController: ModalController,
    private store: Store<State>) {
    this.store.pipe(select(selectAddresses)).subscribe(addresses => this.addresses = addresses);
    this.store.pipe(select(selectCurrentAddress)).subscribe(address => this.address = address);
    this.store.pipe(select(selectGetUser)).subscribe(user => this.user = user);
  }

  ngOnInit() {
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
  }

  ionViewDidEnter(){
    this.store.select(selectModalidad).subscribe(modalidad => {
      this.modalidad = modalidad;
    });
  }



  goToProfile() {
    this.router.navigate(['auth/profile']);
  }

  closeSession() {
    console.log("Close session");
    this.store.dispatch(clearAuth());
    this.router.navigate(['auth'], { replaceUrl: true });
    // localStorage.clear();
  }

  // ADDRESSES

  async openNewAddressModal() {
    const modal = await this.modalController.create({
      component: NewAddressPage,
    });
    modal.onDidDismiss().then(() => {
    });
    return await modal.present();
  }

  async openOptions(address) {
    const actionSheet = await this.actionSheetController.create({
      header: address.address,
      buttons: [{
        text: 'Seleccionar como actual',
        role: 'destructive',
        icon: 'star',
        handler: () => {
          this.store.dispatch(setCurrentAddress({ address }));
          // this.navCtrl.back();
          this.navCtrl.navigateRoot(['/delivery']);
        }
      }, {
        text: 'Eliminar dirección',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log(address);
          this.store.dispatch(deleteAddress({ address }));
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
}
