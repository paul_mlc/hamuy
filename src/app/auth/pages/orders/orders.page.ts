import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { STRINGS } from 'src/app/shared/constants/delivery.constants';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  orders$: Observable<any>;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.orders$ = this.activatedRoute.data.pipe(
      map(data => {
        if (data.orders.length) {
          return data.orders.map(order => {
            const o = order;
            o.shop = JSON.parse(order.shop);
            o.progress = STRINGS.progress[o.state];
            return o;
          });
        }
      })
    );
  }

  ngOnInit() {
  }

  goToOrder(orderId, status) {
    // if (status === 'delivered' || status === 'cancelled' || status === 'rejected') {
    //   // TODO: detalle
    // } else {
    this.router.navigate(['delivery/order', { orderId }]);
    // }
  }
}
