import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tyc',
  templateUrl: './tyc.page.html',
  styleUrls: ['./tyc.page.scss'],
})
export class TycPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  openCategory() {
    this.router.navigateByUrl('services/category');
  }
}
