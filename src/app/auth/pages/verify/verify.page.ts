import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { verify } from '../../store/auth.actions';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { getAuthState } from '../../store';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.page.html',
  styleUrls: ['./verify.page.scss'],
})
export class VerifyPage implements OnInit {

  verifyForm: FormGroup;
  phone;
  errorMessage: string = "";

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private store: Store<State>) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => {
      this.phone = data.phone;
      console.log(data.phone);
      this.verifyForm = this.createPhoneForm();

      // this.store.select(getAuthState)
      // .subscribe((state: any) => {
      //   this.errorMessage = state.error;
      // });
    });
  }

  createPhoneForm(): FormGroup {
    return this.fb.group({
      phone: [this.phone, [Validators.required]],
      verification_code: ['', [Validators.required, Validators.pattern('[0-9]{6}')]],
    });
  }

  sendVerification() {
    const verifyDto = Object.assign({}, this.verifyForm.value);
    this.store.dispatch(verify(verifyDto));
  }
}
