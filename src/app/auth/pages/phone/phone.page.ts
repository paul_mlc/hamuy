import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { UserService } from '../../services/user.service';
import { Store } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { signInWithPhone } from '../../store/auth.actions';
import { Router } from '@angular/router';
@Component({
  selector: 'app-phone',
  templateUrl: './phone.page.html',
  styleUrls: ['./phone.page.scss'],
})
export class PhonePage implements OnInit {
  phoneForm: FormGroup;
  botonDisabled: boolean = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private store: Store<State>,
    private alertController: AlertController,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.phoneForm = this.createPhoneForm();
  }

  ionViewWillEnter(){
    this.botonDisabled = false;
  }

  createPhoneForm(): FormGroup {
    return this.fb.group({
      // phone: ['', [Validators.required, Validators.pattern('[0-9]{10}')]],
      phone: ['', [Validators.required, Validators.pattern('[0-9]{9}')]],
    });
  }

  sendSMS() {
    const phone = '+51' + this.phoneForm.controls.phone.value;
    // let loader = this.loadingController.create(
    //   {
    //     message: 'Conectando...',
    //     duration: 10000
    //   });

    // loader.present();
    this.botonDisabled = true;
    this.userService.sendSMS({ phone })
      .subscribe((data) => {
        // loader.dismiss();
        console.log(data);
        this.botonDisabled = false;
        if (data.success == true) {
          this.router.navigate(['auth/verify', { phone }]);
        } else {
          this.presentAlert();
          return false;
        }

      },
      (error) => {
        this.botonDisabled = false;
       }
      );
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: '¡Atención!',
      mode: 'ios',
      message: 'El número de teléfono no se encuentra registrado.',
      buttons: [
        {
          text: 'Ok',
        }
      ]
    });

    await alert.present();
  }

}
