import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ModalController } from '@ionic/angular';
import { SignUpModalPage } from '../../components/signup-modal/signup-modal.component';
import { Store } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { signUp } from '../../store/auth.actions';
import { getAuthState } from '../../store';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  signUpForm;
  fbtoken;

  constructor(
    private fb: FormBuilder,
    private store: Store<State>,
    private userService: UserService,
    private modalController: ModalController,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.signUpForm = this.createSignUpForm();
    this.store.select(getAuthState)
      .subscribe((state: any) => {
        this.fbtoken = state.fbtoken;
      });
  }

  onSubmit() {
    const user = Object.assign({}, this.signUpForm.value);
    user.phone = '+51' + user.phone;
    user.fbtoken = this.fbtoken;
    user.oneSignalUserId = localStorage.getItem('oneSignalUserId');
    this.store.dispatch(signUp(user));
  }

  createSignUpForm() {
    return this.fb.group({
      name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.pattern('[0-9]{9}')]],
      email: ['', [Validators.email]],
      password: ['', [Validators.required]],
      confirmPassword: []
    },
      { validator: this.checkPassword('password', 'confirmPassword') }
    );
  }

  checkPassword(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }
      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

}
