import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-invoicing',
  templateUrl: './invoicing.page.html',
  styleUrls: ['./invoicing.page.scss'],
})
export class InvoicingPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  openCategory() {
    this.router.navigateByUrl('services/category');
  }
}
