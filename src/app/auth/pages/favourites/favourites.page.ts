import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../../delivery/store';
import { getFavourites } from '../../store/auth.actions';
import { selectFavourites } from '../../store';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.page.html',
  styleUrls: ['./favourites.page.scss'],
})
export class FavouritesPage implements OnInit {

  baseUrl = environment.image_base_url;
  favourites = [];
  cartLength:number=0;

  constructor(
    private router: Router,
    private store: Store<State>
  ) {
    this.store.select(selectFavourites).subscribe(favourites => this.favourites = favourites.map(f => f.shop));
  }

  ngOnInit() {
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
  }

  ionViewDidEnter() {
    this.store.dispatch(getFavourites());
  }

  openShop(shop) {
    this.router.navigate(['delivery/products',
      { shopId: shop.id, name: shop.name, description: shop.description }]);
  }
}
