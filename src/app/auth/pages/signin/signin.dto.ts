export interface SignInDto {
    email: string;
    password: string;
    fbtoken: string;
}
