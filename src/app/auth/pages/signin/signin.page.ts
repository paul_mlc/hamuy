import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getAuthState } from '../../store';
import { SignInDto } from './signin.dto';
import { signIn } from '../../store/auth.actions';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from 'src/app/app.reducer';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {
  signInForm: FormGroup;
  //errorMessage: any;
  fbtoken: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private store: Store<State>) {
      console.log('SigninPage');
      console.log('localstorage', localStorage);
    }

  ngOnInit() {
    this.signInForm = this.createSignInForm();
    this.store.select(getAuthState)
      .subscribe((state: any) => {
        //this.errorMessage = state.error;
        this.fbtoken = state.fbtoken;
      });
  }

  signIn() {
    const signInDto: SignInDto = Object.assign({}, this.signInForm.value);
    signInDto.fbtoken = this.fbtoken;
    this.store.dispatch(signIn(signInDto));
    console.log('signIn localStorage', localStorage);
  }

  createSignInForm(): FormGroup {
    return this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(30)]],
    });
  }

  goToSignUp() {
    this.router.navigate(['auth/signup']);
  }

  goToHome() {
    //this.router.navigate(['delivery']);
  }


}
