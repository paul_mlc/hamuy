import {
  ActionReducer,
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
import { storeLogger } from 'ngrx-store-logger';

import { environment } from '../environments/environment';
import { reducer as AuthReducer } from './auth/store/auth.reducer';
import { reducer as DeliveryReducer } from './delivery/store/delivery.reducer';
import { Auth as AuthState } from './auth/auth.interface';
import { Delivery as DeliveryState } from './delivery/delivery.interface';

export interface State {
  auth: AuthState;
  delivery: DeliveryState;
}

export const reducers: ActionReducerMap<State> = {
  auth: AuthReducer,
  delivery: DeliveryReducer,
};

export function logger(reducer: ActionReducer<State>): any {
  return storeLogger()(reducer);
}

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
    keys: ['auth', 'delivery'],
    rehydrate: true,
    storageKeySerializer: key => `hamuyapp_${key}`
  })(reducer);
}

export const metaReducers: MetaReducer<State>[] =
  !environment.production ? [localStorageSyncReducer, logger] : [localStorageSyncReducer];
