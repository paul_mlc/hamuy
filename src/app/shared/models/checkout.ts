export interface Checkout {
    address_id: number;
    shop_id: number;
    products: Array<CheckoutProduct>;
    user_payment_method_id: number;
    payment_token: string;
    payment_id: string;
}

export interface CheckoutProduct {
    id: number;
    quantity: number;
}
