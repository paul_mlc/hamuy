export const CONFIG = {
    intervalSeconds: 5000
};
export const STRINGS = {
    status: {
        new: 'Estamos procesando tu orden.',
        created: 'Buscando un conductor.',
        accepted: 'El conductor se dirige hacia ti.',
        arrived: 'El conductor llegó.',
        way: 'Ya estás en camino.',
        finished: 'Has llegado a tu destino.',
        'cancelled-user': 'Cancelaste la orden.',
        'cancelled-employee': 'La orden fue rechazada por el conductor.',
    },
    progress: {
        created: 0.15,
        accepted: 0.35,
        taken: 0.50,
        hold: 0.70,
        way: 0.85,
        delivered: 1,
        cancelled: 0,
        rejected: 0,
    }
};
