export const CONFIG = {
    userPaymentMethodID: 1
};
export const STRINGS = {
    order: {
        generating: 'Generando orden...',
        canceled: 'La orden ha sido cancelada.'
    },
    status: {
        created: 'Estamos procesando tu orden.',
        accepted: 'Tu orden ha sido confirmada.',
        taken: 'El motorizado se dirige hacia el restaurante.',
        hold: 'El motorizado está esperando en el restaurante.',
        way: 'El motorizado está en camino.',
        delivered: 'Tu orden ha sido entregada.',
        cancelled: 'Cancelaste la orden.',
        arrived: 'El motorizado llegó y está esperando afuera.',
        rejected: 'La orden fue rechazada por el restaurante.',
    },
    progress: {
        created: 0.15,
        accepted: 0.35,
        taken: 0.50,
        hold: 0.70,
        way: 0.85,
        arrived: 0.95,
        delivered: 1,
        cancelled: 0,
        rejected: 0,
    }
};
export const CATEGORIES = {
    comida: 1,
    tiendas: 2,
    farmacias: 3,
    supermercados: 4,
    bebidas: 5,
    courier: 6,
    promociones: 7
};
