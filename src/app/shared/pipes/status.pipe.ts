import { Pipe, PipeTransform } from '@angular/core';
import { STRINGS } from '../constants/delivery.constants';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: any): any {
    return STRINGS.status[value];
  }

}
