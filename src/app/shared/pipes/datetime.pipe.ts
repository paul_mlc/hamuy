import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'datetime'
})
export class DatetimePipe implements PipeTransform {

  transform(value: any) {
    const messageTime: Date = new Date(value);
    const dateString = this.formatDate(messageTime);

    let result = '';
    result = `${(messageTime.getHours() < 10) ? '0' + messageTime.getHours() : messageTime.getHours()}:${(messageTime.getMinutes() < 10) ?
      '0' + messageTime.getMinutes() : messageTime.getMinutes()}`;

    return dateString + ' ' + this.tConv24(result);
  }

  tConv24(time24) {
    let ts = time24;
    const H = +ts.substr(0, 2);
    let h: any = (H % 12) || 12;
    h = (h < 10) ? ('0' + h) : h;  // leading 0 at the left for 1 digit hours
    const ampm = H < 12 ? ' AM' : ' PM';
    ts = h + ':' + ts.substr(3, 2) + ampm;
    return ts;
  }

  formatDate(_date: Date, separator = '/') {
    const day = _date.getDate();
    const month = _date.getMonth() + 1;
    const year = _date.getFullYear();

    if (month < 10) {
      return `${day}${separator}0${month}${separator}${year}`;
    } else {
      return `${day}${separator}${month}${separator}${year}`;
    }
  }
}
