import { Pipe, PipeTransform } from '@angular/core';
import { STRINGS } from '../constants/taxi.constants';

@Pipe({
  name: 'taxistatus'
})
export class TaxiStatusPipe implements PipeTransform {

  transform(value: any): any {
    return STRINGS.status[value];
  }

}
