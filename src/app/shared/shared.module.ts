import { NgModule, LOCALE_ID, Directive } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ModalPage } from './components/modal/modal.component';
import { RatePage } from './components/rate/rate.component';
import { GeolocationComponent } from './components/geolocation/geolocation.component';
import { SearchComponent } from './components/search/search.component';
import { AddressesDirective } from './directives/addresses.directive';
import { StatusPipe } from './pipes/status.pipe';
import { TaxiStatusPipe } from './pipes/taxi-status.pipe';
import { TimePipe } from './pipes/time.pipe';
import { DatetimePipe } from './pipes/datetime.pipe';
import { NewAddressPage } from './components/new-address/new-address.component';
import { FooterComponent } from './components/footer/footer.component';
import { SupportComponent } from './components/support/support.component';

@NgModule({
    declarations: [
        HeaderComponent,
        FooterComponent,
        ModalPage,
        RatePage,
        GeolocationComponent,
        SearchComponent,
        AddressesDirective,
        StatusPipe,
        TaxiStatusPipe,
        TimePipe,
        DatetimePipe,
        NewAddressPage,
        SupportComponent
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        IonicModule
    ],
    exports: [
        HeaderComponent,
        FooterComponent,
        GeolocationComponent,
        ModalPage,
        RatePage,
        SearchComponent,
        AddressesDirective,
        StatusPipe,
        TaxiStatusPipe,
        TimePipe,
        DatetimePipe,
        NewAddressPage,
        SupportComponent
    ],
    providers: [
    ],
    entryComponents: [
        ModalPage,
        RatePage,
        NewAddressPage,
        SupportComponent
    ]
})
export class SharedModule {
}
