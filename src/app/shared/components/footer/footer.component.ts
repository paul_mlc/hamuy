import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';



@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  @Input() modalidad: number = 0;
  @Input() cartLength: number = 0;

  constructor(private router: Router,
              private navCtrl: NavController,) {
  }

  ngOnInit() {
  }

  goToCart() {
    this.router.navigate(['delivery/cart']);
  }

  goToHome() {
    this.router.navigate(['delivery']);
  }

  goToOrders() {
    this.router.navigate(['delivery/orders']);
  }

  goToSearch() {
    this.router.navigate(['delivery/search']);
  }

  goToDisponibles() {
    this.router.navigate(['delivery/ordersm']);
    //this.router.navigateByUrl('/delivery/ordersm');
    //this.navCtrl.navigateRoot(['delivery/ordersm']);
  }

  goToFavourites() {
    this.router.navigate(['auth/favourites']);
  }

  goToOrdersTaken() {
    this.router.navigate(['delivery/orderstaken']);
  }

  goToCuentasM() {
    this.router.navigate(['delivery/estadocuenta']);
  }

  goToShopNuevos() {
    this.router.navigate(['delivery/shopnuevos']);
  }

  goToShopTaken() {
    this.router.navigate(['delivery/shoptaken']);
  }

  


  

}