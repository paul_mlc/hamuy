import { Component, Output, Input } from '@angular/core';
import { EventEmitter } from 'events';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'rate-page',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss'],
})
export class RatePage {
  // @Output() confirmation = new EventEmitter();
  @Input() driver: any;
  comment = '';
  stars = {
    s1: 'star-outline',
    s2: 'star-outline',
    s3: 'star-outline',
    s4: 'star-outline',
    s5: 'star-outline'
  };
  rating;
  constructor(private modalCtrl: ModalController) {

  }

  rate(n) {
    this.rating = n;
    for (let index = 1; index <= n; index++) {
      this.stars['s' + index] = 'star';
    }
  }

  dismiss() {
    this.modalCtrl.dismiss({
      rating: this.rating,
      commentary: this.comment
    });
  }
}
