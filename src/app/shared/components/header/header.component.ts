import { Component, OnInit, Input } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { SupportComponent } from '../support/support.component';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { selectCurrentAddress } from 'src/app/auth/store';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() light = false;
  @Input() trans: boolean;
  @Input() showAddress = true;
  address: any = null;

  constructor(
    private navCtrl: NavController,
    private modalController: ModalController,
    private store: Store<State>
  ) {
    this.store.pipe(select(selectCurrentAddress)).pipe(filter(o => !!o)).subscribe(address => this.address = address.address.split(',')[0]);
  }

  ngOnInit() { }

  backRouter() {
    this.navCtrl.back();
  }

  async modalSupport() {
    const modal = await this.modalController.create({
      component: SupportComponent, componentProps: {}, cssClass: 'support-modal'
    });

    return await modal.present();
  }
}
