import { Component, Output, Input } from '@angular/core';
import { EventEmitter } from 'events';
import { ModalController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'modal-page',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalPage {
  @Input() type;
  @Input() orderId;
  // @Output() confirmation = new EventEmitter();

  constructor(
    private router: Router,
    private modalCtrl: ModalController,
    private navCtrl: NavController,) {
  }

  ngOnInit() {
    console.log('type: ' + this.type);
  }

  redirectTo() {
    // this.onAction.emit(this.inChat);
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }

  goToOrder() {
    this.dismiss();
    //this.navCtrl.navigateRoot(['/delivery']);
    //this.router.navigate(['delivery/order', { orderId: this.orderId }]);
    
  }
}
