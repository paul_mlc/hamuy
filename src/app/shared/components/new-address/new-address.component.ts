import { Component } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { Subject } from 'rxjs';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { map, debounceTime } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { GmapsService } from 'src/app/shared/services/gmaps.service';
import { State } from 'src/app/app.reducer';
import { addAddress } from 'src/app/auth/store/auth.actions';
declare var google;

@Component({
  selector: 'new-address-page',
  templateUrl: './new-address.component.html',
  styleUrls: ['./new-address.component.scss'],
})
export class NewAddressPage {
  address = '';
  addresses;
  selectedAddress = null;
  map;
  centerMoved$ = new Subject<any>();

  constructor(
    private geolocation: Geolocation,
    private gmaps: GmapsService,
    private modalCtrl: ModalController,
    private store: Store<State>,
    private alertController: AlertController
  ) { }

  ionViewDidEnter() {
    this.setMapConfiguration().then(() => {
      this.geolocation.getCurrentPosition().then(resp => {
        this.map.setCenter(new google.maps.LatLng(+resp.coords.latitude, +resp.coords.longitude));
        this.map.setZoom(17);
      }).catch(() => {
        this.alertOnGPSFails();
      });
    });

    this.centerMoved$.asObservable().pipe(debounceTime(1000)).subscribe(center => {
      // if (this.selectedAddress !== null) {
      this.gmaps.geocoding({ latlng: `${center.lat},${center.lng}` })
        .subscribe(geocodingResp => {
          if (geocodingResp.status === 'OK') {
            this.address = geocodingResp.results[0].formatted_address;
            this.selectedAddress = {
              latitude: center.lat,
              longitude: center.lng,
              address: geocodingResp.results[0].formatted_address,
              place_id: geocodingResp.results[0].place_id
            };
          }
        });
      console.log('selected address', this.selectedAddress);
      // }
    });
  }

  selectAddress(address = null) {
    if (address == null) { this.modalCtrl.dismiss(null); return; }
    this.gmaps.geocoding({ place_id: address.place_id })
      .pipe(
        map(resp => resp.results[0])
      )
      .subscribe(async resp => {
        this.addresses = [];
        this.address = resp.formatted_address;
        this.selectedAddress = {
          address: resp.formatted_address,
          latitude: resp.geometry.location.lat,
          longitude: resp.geometry.location.lng,
          place_id: resp.place_id
        };

        this.map.setCenter(new google.maps.LatLng(+this.selectedAddress.latitude, +this.selectedAddress.longitude));
        this.map.setZoom(17);
        console.log(this.selectedAddress);
      });
  }


  getAutocompleteList($event) {
    if ($event.target.value.length < 4) {
      return false;
    }
    this.clearAddress();
    this.gmaps.autocomplete($event.target.value)
      .pipe(
        map(list => list.predictions.map(
          p => ({ address: p.description, place_id: p.place_id })
        ))
      )
      .subscribe(list => this.addresses = list);
  }

  setMapConfiguration(): Promise<any> {
    return new Promise(resolve => {
      const mapEl: HTMLElement = document.getElementById('map');
      this.map = new google.maps.Map(mapEl, {
        disableDefaultUI: true,
        center: new google.maps.LatLng(-12.067838, -75.210126),
        zoom: 11,
        zoomControl: false,
        styles: [
          {
            featureType: 'poi',
            stylers: [
              { visibility: 'off' }
            ]
          }
        ]
      });
      google.maps.event.addListener(this.map, 'center_changed', () => {
        const center = this.map.getCenter();
        this.centerMoved$.next({
          lat: center.lat(),
          lng: center.lng()
        });
      });
      resolve();
    });
  }

  clearAddress() {
    if (this.selectedAddress !== null) {
      this.address = '';
      this.selectedAddress = null;

      this.map.setCenter(new google.maps.LatLng(-12.067838, -75.210126));
      this.map.setZoom(11);
    }
  }

  saveAddress() {
    this.store.dispatch(addAddress(this.selectedAddress));
    this.dismiss();
  }

  dismiss() {
    this.modalCtrl.dismiss(null);
  }

  async alertOnGPSFails() {
    const alert = await this.alertController.create({
      header: 'Oops',
      message: 'Parece que tu gps está desactivado o no le diste permisos a la aplicación.',
      mode: 'ios',
      buttons: ['OK']
    });

    await alert.present();
  }
}
