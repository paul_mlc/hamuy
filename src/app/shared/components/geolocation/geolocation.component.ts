import { Component, OnInit } from '@angular/core';
import { State } from 'src/app/app.reducer';
import { Store, select } from '@ngrx/store';
import { selectCurrentAddress } from 'src/app/auth/store';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-geolocation',
  templateUrl: './geolocation.component.html',
  styleUrls: ['./geolocation.component.scss'],
})
export class GeolocationComponent implements OnInit {
  address;
  constructor(private store: Store<State>) { }

  ngOnInit() {
    this.store.pipe(select(selectCurrentAddress), filter(a => !!a))
      .subscribe(current => this.address = current.address);
  }
}
