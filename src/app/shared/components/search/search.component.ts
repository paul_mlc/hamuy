import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-search-input',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  @Input() typeTwo = false;
  @Input() placeholder = '¿Qué buscas?';

  constructor(private navCtrl: NavController) { }

  ngOnInit() { }

  backRouter() {
    this.navCtrl.back();
  }

  goHome() {
    this.navCtrl.navigateRoot(['/home']);
  }
}
