import { Directive, HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Directive({
  selector: '[appAddresses]'
})
export class AddressesDirective {
  constructor(
    private router: Router
  ) { }

  @HostListener('click') onClick() {
    this.router.navigate(['auth/addresses']);
  }
}
