import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GmapsService {

  constructor(private httpService: HttpService) { }

  public autocomplete(query: string): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/autocomplete/' + query);
  }

  public geocoding(params): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/geocoding', params);
  }

}
