import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TaxiService {

  constructor(private httpService: HttpService) { }

  public getPredictedAddresses(address: { address: string }) {
    return this.httpService.get(environment.api_base_url + '/predictedAddresses/', address);
  }

  public getPrice(positions) {
    return this.httpService.post(environment.api_base_url + '/travel/price/', positions);
  }

  public getTravel() {
    return this.httpService.get(environment.api_base_url + '/travel/');
  }

  public getCurrentTravel() {
    return this.httpService.get(environment.api_base_url + '/travel?resume=1');
  }

  public requestTaxi(request) {
    return this.httpService.post(environment.api_base_url + '/travel/store', request);
  }

  public cancelRequest(request) {
    return this.httpService.post(environment.api_base_url + '/travel/cancelled-user/update', request);
  }

  public getCurrentDriverPosition(employeeId) {
    return this.httpService.get(environment.api_base_url + '/driverPosition/' + employeeId);
  }

  public getCurrentEmployee(employeeId) {
    return this.httpService.get(environment.api_base_url + '/employee/' + employeeId);
  }

  public rateTravel(request) {
    return this.httpService.post(environment.api_base_url + '/reviewTravel', request);
  }

}
