import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class StorageService {


    private onStorage: BehaviorSubject<any>;

    constructor(private storage: Storage) {
        this.onStorage = new BehaviorSubject<any>({});
        this.storage.get('address').then(address => {
            this.setValue('address', address);
        });
    }

    setValue(key: string, value: any) {
        this.storage.set(key, value);
        this.onStorage.next(value);
    }

    getValue() {
        this.onStorage.next(true);
    }

    observeStorage(): Observable<any> {
        return this.onStorage.asObservable();
    }
}
