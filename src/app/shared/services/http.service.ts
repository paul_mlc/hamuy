import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  public delete(apiUrl: string, objParams: any = {}): Observable<any> {
    let reqOptions: any = {};
    if (!reqOptions) {
      reqOptions = {
        params: new HttpParams()
      };
    }

    if (objParams) {
      reqOptions.params = new HttpParams();
      for (const key in objParams) {
        if (objParams.hasOwnProperty(key)) {
          reqOptions.params = reqOptions.params.set(key, objParams[key]);
        }
      }
    }

    return this.http.delete(`${apiUrl}`, reqOptions);
  }

  public get(apiUrl: string, objParams: object = {}, reqOptions: any = {}): Observable<any> {
    if (!reqOptions) {
      reqOptions = {
        params: new HttpParams()
      };
    }

    if (objParams) {
      reqOptions.params = new HttpParams();
      for (const key in objParams) {
        if (objParams.hasOwnProperty(key)) {
          reqOptions.params = reqOptions.params.set(key, objParams[key]);
        }
      }
    }

    return this.http.get(`${apiUrl}`, reqOptions);
  }

  public post(apiUrl: string, body?: object | string, reqOptions: any = {}): Observable<any> {
    return this.http.post(`${apiUrl}`, body, reqOptions);
  }

  public put(apiUrl: string, body: object = {}, reqOptions: any = {}): Observable<any> {
    return this.http.put(`${apiUrl}`, body, reqOptions);
  }

}
