import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError, from } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';

import { AlertController, Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';


const TOKEN_KEY = 'hamuyapp_auth';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

    protected url = environment;
    protected debug = true;

    constructor(private alertController: AlertController, private platform: Platform) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const token = JSON.parse(window.localStorage.getItem(TOKEN_KEY)).access_token;

        if (token) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        }

        if (!request.headers.has('Content-Type')) {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        }

        if (!request.headers.has('App-Version')) {
            request = request.clone({ headers: request.headers.set('App-Version', environment.version) });
        }

        if (!request.headers.has('App-Type')) {
            request = request.clone({ headers: request.headers.set('App-Type', 'client') });
        }

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // do nothing for now
                }
                return event;
            }),
            catchError((httpErrorResponse: HttpErrorResponse) => {
                const { status, error } = httpErrorResponse;
                let reason = '';
                if (status === 401) {
                    reason = error.message;
                    this.presentAlert(status, reason);
                }

                if (status === 403) {
                    reason = error.message;
                    this.presentUpdateAlert(status, reason);
                }
                if (status === 422) {
                    reason = Object.values(error.errors).join();
                    this.presentAlert(status, reason);
                }
                if (status === 500) {
                    reason = error.message;
                    this.presentAlert(status, reason);
                }

                return throwError(error);
            })
        );
    }

    async presentAlert(status, reason) {

        if (reason.includes('[HTTP 400] Unable to create record: Invalid parameter') == true) {
            // El númeor está registrado en la BBDD pero no corresponde a un número válido
            reason = 'El número de teléfono es incorrecto.';
        } else if (reason.includes('phone number is not currently reachable via SMS or MMS') == true) {
            // La compañía telefónica no soporta el servicio de Twilio
            reason = 'No es posible enviar un SMS a tu teléfono. Vuelva atrás e ingrese via Email.';
        }
        
        const alert = await this.alertController.create({
            cssClass: 'alert-error',
            header: 'Oops!',
            mode: 'ios',
            message: reason,
            buttons: ['OK']
        });

        await alert.present();
    }

    async presentUpdateAlert(status, reason) {
        const alert = await this.alertController.create({
            cssClass: 'alert-error',
            header: 'Oops!',
            mode: 'ios',
            message: reason,
            buttons: [{
                text: 'Actualizar',
                handler: () => {
                    window.open('https://play.google.com/store/apps/details?id=pe.bigoo.app', '_system', 'location=yes');
                }
            }]
        });

        await alert.present();
    }
}
