import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Store, select } from '@ngrx/store';
import { State } from './app.reducer';
import { selectIsLoggedIn, selectGetAuth } from './auth/store';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  signal_app_id: string = '4d4544f2-51a9-4978-aa14-e745ed378e7d';
  firebase_id: string = '793976930059';
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Inbox',
      url: '/folder/Inbox',
      icon: 'mail'
    },
    {
      title: 'Outbox',
      url: '/folder/Outbox',
      icon: 'paper-plane'
    },
    {
      title: 'Favorites',
      url: '/folder/Favorites',
      icon: 'heart'
    },
    {
      title: 'Archived',
      url: '/folder/Archived',
      icon: 'archive'
    },
    {
      title: 'Trash',
      url: '/folder/Trash',
      icon: 'trash'
    },
    {
      title: 'Spam',
      url: '/folder/Spam',
      icon: 'warning'
    }
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private store: Store<State>,
    private router: Router,
    public oneSignal: OneSignal
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {

      if (this.platform.is('cordova')) {

        this.oneSignal.startInit(this.signal_app_id, this.firebase_id);
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.handleNotificationReceived().subscribe((res) => {
          // do something when notification is received
          console.log(res);
        });

        this.oneSignal.handleNotificationOpened().subscribe((res) => {
          // do something when a notification is opened
          console.log(res);
          // if (res.notification.payload.additionalData.pagina) {

          //   // Abro el name de la página enviado en additionalData desde One Signal
          //   var page: any;
          //   page = this.pagesArr.find(x => x.name == res.notification.payload.additionalData.pagina);
          //   if (page != null) {
          //     this.nav.setRoot("RubrosPage", { 'pagina': page.component });
          //   }
          // }

        });

        this.oneSignal.endInit();

        // this.oneSignal.getIds().then(identity => {
        // alert(identity.pushToken + " It's Push Token");
        // alert(identity.userId + " It's Devices ID");
        // });
        console.log('tengo oneSignalUserId', localStorage.getItem('oneSignalUserId'));
        if (!localStorage.getItem('oneSignalUserId')) {

          this.oneSignal.getIds().then(identity => {
            console.log('oneSignalUserId', identity.userId);
            localStorage.setItem('oneSignalUserId', identity.userId);
          });

        }
        console.log('LocalStorage app component', localStorage);
      }

      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#666666');
      this.splashScreen.hide();
      this.redirectIfAuthenticated();

    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }

  redirectIfAuthenticated(): void {
    this.store.pipe(select(selectIsLoggedIn), filter(a => a))
      .subscribe(state => {
        this.router.navigate(['delivery']);
      });
  }


}
