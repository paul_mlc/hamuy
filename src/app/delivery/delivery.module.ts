import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePage } from './pages/home/home.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { CourierPage } from './pages/courier/courier.page';
import { ShopsPage } from './pages/shops/shops.page';
import { SubcategoriesPage } from './pages/subcategories/subcategories.page';
import { ProductsPage } from './pages/products/products.page';
import { ProductPage } from './pages/product/product.page';
import { CartPage } from './pages/cart/cart.page';
import { CheckoutPage } from './pages/checkout/checkout.page';
import { OrdersPage } from './pages/orders/orders.page';
import { OrderPage } from './pages/order/order.page';
import { SearchPage } from './pages/search/search.page';
import { SubcategoriesResolverService } from './pages/subcategories/subcategories-resolver.service';
import { ProductsResolverService } from './pages/products/products-resolver.service';
import { OrdersResolverService } from './pages/orders/orders-resolver.service';
import { OrderResolverService } from './pages/order/order-resolve.service';
import { OrdersmPage } from './pages/ordersm/ordersm.page';
import { OrdersmResolverService } from './pages/ordersm/ordersm-resolver.service';
import { OrderstakenResolverService } from './pages/orderstaken/orderstaken-resolver.service';
import { OrderstakenPage } from './pages/orderstaken/orderstaken.page';
import { OrdertakePage } from './pages/ordertake/ordertake.page';
import { ShopnuevosPage } from './pages/shopnuevos/shopnuevos.page';
import { ShopnuevosResolverService } from './pages/shopnuevos/shopnuevos-resolver.service';
import { ShoptakenResolverService } from './pages/shoptomados/shoptaken-resolver.service';
import { ShoptakePage } from './pages/shoptake/shoptake.page';
import { ShoptakenPage } from './pages/shoptomados/shoptaken.page';
import { EstadocuentaPage } from './pages/estadocuenta/estadocuenta.page';
import { MapashopPage } from './pages/mapashop/mapashop.page';
import { EstadocuentaResolverService } from './pages/estadocuenta/estadocuenta-resolver.service';
import { MercadoPagoPage } from './pages/mercado-pago/mercado-pago.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  },
  {
    path: 'courier',
    component: CourierPage
  },
  {
    path: 'subcategories',
    component: SubcategoriesPage,
    resolve: {
      subcategories: SubcategoriesResolverService
    }
  },
  {
    path: 'shops',
    component: ShopsPage
  },
  {
    path: 'products',
    component: ProductsPage,
    resolve: {
      products: ProductsResolverService
    }
  },
  {
    path: 'product',
    component: ProductPage
  },
  {
    path: 'cart',
    component: CartPage
  },
  {
    path: 'checkout',
    component: CheckoutPage
  },
  {
    path: 'mercadopago',
    component: MercadoPagoPage 
  },
  {
    path: 'estadocuenta',
    component: EstadocuentaPage,
    resolve: {
      estadocuenta: EstadocuentaResolverService
    }
  },
  {
    path: 'orders',
    component: OrdersPage,
    resolve: {
      orders: OrdersResolverService
    }
  },
  {
    path: 'ordersm',
    component: OrdersmPage,
    resolve: {
      ordersm: OrdersmResolverService
    }
  },
  {
    path: 'orderstaken',
    component: OrderstakenPage,
    resolve: {
      orderstaken: OrderstakenResolverService
    }
  },
  {
    path: 'order',
    component: OrderPage,
    resolve: {
      order: OrderResolverService
    }
  },
  {
    path: 'ordertake',
    component: OrdertakePage
  },
  {
    path: 'search',
    component: SearchPage
  },
  {
    path: 'mapashop',
    component: MapashopPage
  },
  {
    path: 'shopnuevos',
    component: ShopnuevosPage,
    resolve: {
      shopnuevos: ShopnuevosResolverService
    }
  },
  {
    path: 'shoptaken',
    component: ShoptakenPage,
    resolve: {
      shoptaken: ShoptakenResolverService
    }
  },
  {
    path: 'shoptake',
    component: ShoptakePage
  }
];

@NgModule({
  declarations: [
    HomePage,
    CourierPage,
    ShopsPage,
    SubcategoriesPage,
    ProductsPage,
    ProductPage,
    CartPage,
    CheckoutPage,
    MercadoPagoPage,
    OrdersPage,
    OrderPage,
    SearchPage,
    OrdersmPage,
    OrderstakenPage,
    OrdertakePage,
    ShopnuevosPage,
    ShoptakenPage,
    ShoptakePage,
    EstadocuentaPage,
    MapashopPage
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})
export class DeliveryModule { }
