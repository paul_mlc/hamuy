import { createAction, props } from '@ngrx/store';

export enum DeliveryActionTypes {
    ClearDelivery = '[Delivery] Clear Delivery',
    AddProductToCart = '[Delivery - Cart] Add Product To Cart',
    RemoveProductFromCart = '[Delivery - Cart] Remove Product from Cart',
    UpdateProductFromCart = '[Delivery - Cart] Update Product from Cart',
    // SignIn = '[Auth Page] Sign In',
    // SignInSuccess = '[Auth API] Sign In Success',
    // SignInFailure = '[Auth API] Sign In Failure',

}

export const clearDelivery = createAction(
    DeliveryActionTypes.ClearDelivery
);

export const addProductToCart = createAction(
    DeliveryActionTypes.AddProductToCart,
    props<{ shopId: any, product: any, quantity: number }>()
);
export const removeProductFromCart = createAction(
    DeliveryActionTypes.RemoveProductFromCart,
    props<{ item: any }>()
);
export const updateProductFromCart = createAction(
    DeliveryActionTypes.UpdateProductFromCart,
    props<{ item: any }>()
);

// export const signInSuccess = createAction(
//     AuthActionTypes.SignInSuccess,
//     props<any>()
// );
// export const signInFailure = createAction(
//     AuthActionTypes.SignInFailure,
//     props<any>()
// );
