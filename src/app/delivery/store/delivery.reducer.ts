import * as DeliveryActions from './delivery.actions';
import { Delivery as DeliveryState } from '../delivery.interface';
import { on, Action, createReducer } from '@ngrx/store';

export const initialState: DeliveryState = {
    error: null,
    shopId: "",
    cart: []
};

const deliveryReducer = createReducer(
    initialState,
    on(DeliveryActions.clearDelivery, () => initialState),
    on(DeliveryActions.addProductToCart, (state, payload) => {
        const shopId = payload.shopId;
        const items = [...state.cart];
        let pPosition;
        const product = items.find((p, i) => {
            if (p.id === payload.product.id) {
                pPosition = i;
                return true;
            }
            return false;
        });
        if (product) {
            items[pPosition] = {
                id: payload.product.id,
                quantity: items[pPosition].quantity + payload.quantity,
                product: payload.product
            };
        } else {
            items.push({
                id: payload.product.id,
                quantity: payload.quantity,
                product: payload.product
            });
        }
        return ({ ...state, shopId: shopId, cart: items });
    }),
    on(DeliveryActions.removeProductFromCart, (state, payload) => {
        const products = [...state.cart];
        let pPosition;
        products.find((p, i) => {
            if (p.id === payload.item.id) {
                pPosition = i;
                return true;
            }
            return false;
        });
        products.splice(pPosition, 1);
        if(products.length > 0) {
            return ({ ...state, cart: products });
        } else {
            return ({ ...state, shopId: '', cart: products });
        }
        
    }),
    on(DeliveryActions.updateProductFromCart, (state, payload) => {
        const products = [...state.cart];
        let pPosition;
        products.find((p, i) => {
            if (p.id === payload.item.id) {
                pPosition = i;
                return true;
            }
            return false;
        });
        products[pPosition] = payload.item;
        return ({ ...state, cart: products });
    }),
);

export function reducer(state: DeliveryState, action: Action) {
    return deliveryReducer(state, action);
}
