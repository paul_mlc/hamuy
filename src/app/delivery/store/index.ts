import { createSelector, createFeatureSelector } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { Delivery as DeliveryState } from '../delivery.interface';
// export const selectAuth = (state: State) => state.auth;
export const getDeliveryState = createFeatureSelector<State, DeliveryState>('delivery');

export const selectGetCart = createSelector(
    getDeliveryState,
    (state: DeliveryState) => state.cart
);

export const cartLength = createSelector(
    getDeliveryState,
    (state: DeliveryState) => state.cart.length
);

export const shopId = createSelector(
    getDeliveryState,
    (state: DeliveryState) => state.shopId
);

