import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of, throwError, forkJoin } from 'rxjs';
import { catchError, exhaustMap, map, tap, switchMap, concatMap } from 'rxjs/operators';

import * as DeliveryActions from './delivery.actions';

import { LoadingController } from '@ionic/angular';
import { DeliveryService } from '../services/delivery.service';

@Injectable()
export class DeliveryEffects {
    loading;

    constructor(
        private actions$: Actions,
        private deliveryService: DeliveryService
    ) { }

    async presentLoading(message: string) {
        this.loading = await (new LoadingController()).create({
            message,
        });
        await this.loading.present();
    }

    dissmisLoading() {
        this.loading.dismiss();
    }
}
