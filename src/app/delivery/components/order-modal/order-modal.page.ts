import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'order-modal-page',
    templateUrl: './order-modal.page.html',
    styleUrls: ['./order-modal.page.scss'],
})
export class OrderModalPage {

    @Input() order: any;

    constructor(private modalCtrl: ModalController) {

    }

    ionViewDidEnter() {
        console.log(this.order);
        console.log(JSON.parse(this.order.address));
        this.order.address = JSON.parse(this.order.address);
        this.order.products = JSON.parse(this.order.products);
    }

    dismiss() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modalCtrl.dismiss({
            dismissed: true
        });
    }
}
