import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
declare var google;

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
})
export class AddressComponent implements OnInit {
  map;
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
    this.setMapConfiguration();
  }

  dismiss() {
    this.modalCtrl.dismiss(null);
  }

  setMapConfiguration(): Promise<any> {
    return new Promise(resolve => {
      const mapEl: HTMLElement = document.getElementById('map');
      this.map = new google.maps.Map(mapEl, {
        disableDefaultUI: true,
        center: new google.maps.LatLng(-12.067838, -75.210126),
        zoom: 11,
        zoomControl: false,
        styles: [
          {
            featureType: 'poi',
            stylers: [
              { visibility: 'off' }
            ]
          }
        ]
      });
      resolve();
    });

  }
}
