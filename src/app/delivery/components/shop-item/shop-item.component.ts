import { Component, OnInit, Output, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shop-item',
  templateUrl: './shop-item.component.html',
  styleUrls: ['./shop-item.component.scss'],
})
export class ShopItemComponent implements OnInit {

  @Input() public shop: any;
  constructor(private router: Router) { }

  ngOnInit() { }

  openShop(shop) {
    this.router.navigate(['delivery/products',
      { shopId: shop.id, name: shop.name, description: shop.description }]);
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }
}
