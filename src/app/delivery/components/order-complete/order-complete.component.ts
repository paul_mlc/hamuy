import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-order-complete',
  templateUrl: './order-complete.component.html',
  styleUrls: ['./order-complete.component.scss'],
})
export class OrderCompleteComponent implements OnInit {

  constructor(private router: Router, private modalCtrl: ModalController) { }

  ngOnInit() { }

  goToHome() {
    this.modalCtrl.dismiss();
    this.router.navigate(['delivery'], { replaceUrl: true });
  }
}
