import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss'],
})
export class CartItemComponent implements OnInit {
  @Input() public item: any;
  @Output()
  protected minusQuantity: EventEmitter<any> = new EventEmitter();
  @Output()
  protected plusQuantity: EventEmitter<any> = new EventEmitter();
  baseUrl = environment.image_base_url;
  constructor() { }

  ngOnInit() { 
    console.log('item', this.item);
  }

  minus() {
    if (this.item.quantity > 0) {
      this.minusQuantity.emit({ ...this.item, quantity: this.item.quantity - 1 });
    }
  }

  plus() {
    this.plusQuantity.emit({ ...this.item, quantity: this.item.quantity + 1 });
  }
}
