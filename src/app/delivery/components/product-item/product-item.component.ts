import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss'],
})
export class ProductItemComponent implements OnInit {
  @Input() public product: any;
  @Input() public shopId: any;
  @Input() public addProduct: any;
  baseUrl = environment.image_base_url;
  constructor() { }

  ngOnInit() { }

}
