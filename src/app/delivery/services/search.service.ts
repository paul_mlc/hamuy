import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { environment } from 'src/environments/environment';
import { Checkout } from '../../shared/models/checkout';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SearchService {

    constructor(private httpService: HttpService) { }

    public globalSearch(req): Observable<any> {
        return this.httpService.post(environment.api_base_url + '/globalsearch', req);
    }

}
