import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { environment } from 'src/environments/environment';
import { Checkout } from '../../shared/models/checkout';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {

  constructor(private httpService: HttpService) { }

  public getSubCategories(categoryId): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/subcategoryList/' + categoryId);
  }

  public getShops(subCategoryId, addressId): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/shopList/' + subCategoryId + '/' + addressId);
  }

  public getShopsMock(subCategoryId): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/shopList/' + subCategoryId);
  }

  public getShopData(shopId): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/shopData/' + shopId);
  }

  public getProductsByShop(shopId): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/productList/' + shopId);
  }

  public getProductCategoriesByShop(shopId): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/productCategories/' + shopId);
  }

  public getDeliveryFee(request): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/order/deliveryFee', request);
  }

  public getEstimatedTime(request): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/order/estimatedTime', request);
  }

  public shopNear(request): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/order/shopNear', request);
  }

  public postCheckout(checkoutDto: Checkout): Observable<any> {
    return this.httpService.post(environment.api_base_url + '/order/store', checkoutDto);
  }

  public getOrder(orderId: number): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/order/' + orderId + '/detail');
  }

  public cancelOrder(orderId: number): Observable<any> {
    return this.httpService.post(environment.api_base_url + '/order/' + orderId + '/cancelled');
  }

  public rateOrder(request) {
    return this.httpService.post(environment.api_base_url + '/reviewOrder', request);
  }

  public ordersm(): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/ordersm');
  }

  public takeOrder(order): Observable<any> {
    return this.httpService.post(environment.api_base_url + '/takeOrder', order);
  }

  public onWayOrder(order): Observable<any> {
    return this.httpService.post(environment.api_base_url + '/onWayOrder', order);
  }

  public deliverOrder(order): Observable<any> {
    return this.httpService.post(environment.api_base_url + '/deliverOrder', order);
  }

  // public ordertake(order): Observable<any> {
  //   return this.httpService.post(environment.api_base_url + '/acceptOrder/' + order);
  // }
  
  public orderstaken(): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/orderstaken');
  }

  public estadoCuenta(): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/estadocuenta');
  }

  public shopNuevos(): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/shopNuevos');
  }

  public shopTaken(): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/shopTaken');
  }

  public shopTake(order): Observable<any> {
    return this.httpService.post(environment.api_base_url + '/shopAcceptOrder', order);
  }

  public ingresosMotorizado(): Observable<any> {
    return this.httpService.get(environment.api_base_url + '/ingresosMotorizado');
  }
  

}