import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { STRINGS } from 'src/app/shared/constants/delivery.constants';
import { environment } from 'src/environments/environment';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';

@Component({
  selector: 'app-ordertake',
  templateUrl: './ordertake.page.html',
  styleUrls: ['./ordertake.page.scss'],
})
export class OrdertakePage implements OnInit {

  order:any;
  orders$: Observable<any>;
  baseUrl = environment.image_base_url;
  deliveryAddress:string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private deliveryService: DeliveryService
  ) {
    console.log('OrdertakePage');
    this.activatedRoute.params.subscribe(data => {
      console.log(data);
      this.order = JSON.parse(data.order);
      this.deliveryAddress = this.order.address.address;
    });
  
  }

  ngOnInit() {
  }

  takeOrder(order) {

    this.deliveryService.takeOrder(order)
      .subscribe((res) => {
        console.log(res);
        this.router.navigate(['delivery/orderstaken']);
      });
   
   
  }

  locationView() {
    this.router.navigate(['delivery/mapashop', { order: JSON.stringify(this.order) }]);
  }
}
