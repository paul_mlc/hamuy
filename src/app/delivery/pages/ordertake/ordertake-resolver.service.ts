import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class OrdertakeResolverService implements Resolve<any> {
    constructor(private deliveryService: DeliveryService) { }

    resolve(route: ActivatedRouteSnapshot) {
        const order = route.paramMap.get('order');
        return this.deliveryService.takeOrder(order).pipe(map(r => r.orders));
    }
}
