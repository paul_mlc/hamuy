import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { map } from 'rxjs/operators';
import { DeliveryService } from '../../services/delivery.service';

@Injectable({
    providedIn: 'root'
})
export class ShoptakenResolverService implements Resolve<any> {
    constructor(private deliveryService: DeliveryService) { }

    resolve() {
        return this.deliveryService.shopTaken().pipe(map(r => r.orders));
    }
}
