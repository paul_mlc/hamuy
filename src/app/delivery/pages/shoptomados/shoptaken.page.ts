import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { STRINGS } from 'src/app/shared/constants/delivery.constants';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-shoptaken',
  templateUrl: './shoptaken.page.html',
  styleUrls: ['./shoptaken.page.scss'],
})
export class ShoptakenPage implements OnInit {
  orders$: Observable<any>;
  baseUrl = environment.image_base_url;
  verDetalle:Array<boolean>=[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    console.log('ShoptakenPage');
    this.orders$ = this.activatedRoute.data.pipe(
      map(data => {
        console.log("data: ", data);
        if (data.shoptaken.length) {
          this.verDetalle = Array(data.shoptaken.length);
          return data.shoptaken.map(order => {
            const o = order;
            o.shop = JSON.parse(order.shop);
            o.address = JSON.parse(order.address);
            o.product_list = JSON.parse(order.products);
            o.amount = +order.product_cost + +order.cost_shipping;
            o.date = order.order_datetime.substr(8, 2) + '/' + order.order_datetime.substr(5, 2);
            o.time = order.order_datetime.substr(11, 2) + ':' + order.order_datetime.substr(14, 2);
            o.progress = STRINGS.progress[o.state];

            switch(o.state) { 
              case 'accepted': { 
                 o.state='Espera aceptación de motorizado'; 
                 break; 
              } 
              case 'taken': { 
                o.state='Tomado por un motorizado'; 
                break; 
              } 
              case 'hold': { 
                o.state='Hold'; 
                break; 
              } 
              case 'way': { 
              o.state='En camino'; 
              break; 
              }
              case 'arrived': { 
                o.state='El motorizado ha llegado a destino'; 
                break; 
              }
              case 'delivered': { 
                o.state='Orden entregada'; 
                break; 
              }
              case 'cancelled': { 
                o.state='Orden cancelada'; 
                break; 
              }
              case 'rejected': { 
                o.state='Orden rechazada'; 
                break; 
              }
                
              // case constant_expression2: { 
              //    //statements; 
              //    break; 
              // } 
              // default: { 
              //    //statements; 
              //    break; 
              // } 
           } 

            // console.log("o: ",o);
            return o;
            // 2020-08-08 19:48:11"
            // 20-08"
          });
        }
      })
    );
    
  }

  ngOnInit() {
  }

  viewDetails(index) {
    if(this.verDetalle[index]==true) {
      this.verDetalle[index]=false;
    } else {
      this.verDetalle[index]=true;
    }
  }

  goToOrder(orderId, status) {
    // if (status === 'delivered' || status === 'cancelled' || status === 'rejected') {
    //   // TODO: detalle
    // } else {
    this.router.navigate(['delivery/order', { orderId }]);
    // }
  }
}
