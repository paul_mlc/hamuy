import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class OrderstakenResolverService implements Resolve<any> {
    constructor(private userService: DeliveryService) { }

    resolve() {
        return this.userService.orderstaken().pipe(map(r => r.orders));
    }
}
