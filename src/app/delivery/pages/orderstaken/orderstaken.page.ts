import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { STRINGS } from 'src/app/shared/constants/delivery.constants';
import { environment } from 'src/environments/environment';
import { OrdersResolverService } from 'src/app/auth/pages/orders/orders-resolver.service';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';

@Component({
  selector: 'app-orderstaken',
  templateUrl: './orderstaken.page.html',
  styleUrls: ['./orderstaken.page.scss'],
})
export class OrderstakenPage implements OnInit {
  orders$: Observable<any>;
  baseUrl = environment.image_base_url;
  verDetalle:Array<boolean>=[];
  estado: string = "TOMADO";
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private deliveryService: DeliveryService
  ) {
   
    // this.buscarOrdenes();
    
  }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.buscarOrdenes();
  }
  buscarOrdenes(){
    console.log('arranco busqueda');
    this.orders$ = this.activatedRoute.data.pipe(
      map(data => {
        console.log("data: ", data);
        if (data.orderstaken.length) {
          this.verDetalle.push(false);
          let i = 0;
          return data.orderstaken.map(order => {
            const o = order;
            i=i+1;
            o.shop = JSON.parse(order.shop);
            o.address = JSON.parse(order.address);
            o.product_list = JSON.parse(order.products);
            o.amount = +order.product_cost + +order.cost_shipping;
            o.ganancia = +order.commission;
            o.date = order.order_datetime.substr(8, 2) + '/' + order.order_datetime.substr(5, 2);
            o.time = order.order_datetime.substr(11, 2) + ':' + order.order_datetime.substr(14, 2);
            o.progress = STRINGS.progress[o.state];
            switch(o.state) { 
              case 'created': { 
                o.state='EN CURSO'; 
                break; 
             } 
              case 'accepted': { 
                 o.state='ACEPTADO'; 
                 break; 
              } 
              case 'taken': { 
                o.state='TOMADO'; 
                break; 
              } 
              case 'hold': { 
                o.state='HOLD'; 
                break; 
              } 
              case 'way': { 
              o.state='EN CAMINO'; 
              break; 
              }
              case 'arrived': { 
                o.state='ARRIBADO'; 
                break; 
              }
              case 'delivered': { 
                o.state='ENTREGADO'; 
                break; 
              }
              case 'cancelled': { 
                o.state='CANCELADO'; 
                break; 
              }
              case 'rejected': { 
                o.state='RECHAZADO'; 
                break; 
              }
            }
            if (i==1){
              this.estado = o.state;
            }
            console.log('data cambiada', o);
            // console.log('pesos', orders$);
            // console.log("o: ",o);
            return o;
            // 2020-08-08 19:48:11"
            // 20-08"
          });
        }
      })
    );
  }

  viewDetails(index) {
    if(this.verDetalle[index]==false) {
      this.verDetalle[index]=true;
    } else {
      this.verDetalle[index]=false;
    }
  }

  onWayOrder(order) {

    this.deliveryService.onWayOrder(order)
      .subscribe((res) => {
        console.log("onWayOrder");
        console.log(res);
        this.estado = "EN CAMINO";
        console.log('variable', this.estado);
        // this.ionViewWillEnter();
      });

  }

  deliverOrder(order) {

    this.deliveryService.deliverOrder(order)
      .subscribe((res) => {
        console.log("deliverOrder");
        console.log(res);
        this.estado = "ENTREGADO";
      });

  }

  

}
