import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { STRINGS } from 'src/app/shared/constants/delivery.constants';
import { environment } from 'src/environments/environment';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';

@Component({
  selector: 'app-shopnuevos',
  templateUrl: './shopnuevos.page.html',
  styleUrls: ['./shopnuevos.page.scss'],
})
export class ShopnuevosPage implements OnInit {
  orders$: Observable<any>;
  baseUrl = environment.image_base_url;
  verDetalle:Array<boolean>=[];
  

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private deliveryService: DeliveryService
  ) {
    console.log('ShopnuevosPage');
    this.orders$ = this.activatedRoute.data.pipe(
      map(data => {
        console.log("data shop nuevos: ", data);
        if (data.shopnuevos.length) {
          this.verDetalle = Array(data.shopnuevos.length);
          return data.shopnuevos.map(order => {
            const o = order;
            o.shop = JSON.parse(order.shop);
            o.address = JSON.parse(order.address);
            o.product_list = JSON.parse(order.products);
            o.amount = +order.product_cost + +order.cost_shipping;
            o.progress = STRINGS.progress[o.state];
            // console.log("o: ",o);
            return o;
           
          });
        }
      })
    );
    
  }

  ngOnInit() {
  }

  viewDetails(index) {
    if(this.verDetalle[index]==true) {
      this.verDetalle[index]=false;
    } else {
      this.verDetalle[index]=true;
    }
  }

  goToTakeOrder(order) {
    this.deliveryService.shopTake(order)
      .subscribe((res) => {
        console.log(res);
        this.router.navigate(['delivery/shoptaken']);
      });
  }

  
}