import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
//import { UserService } from 'src/app/auth/services/user.service';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ShopnuevosResolverService implements Resolve<any> {
    constructor(private deliveryService: DeliveryService) { }

    resolve() {
        return this.deliveryService.shopNuevos().pipe(map(r => r.orders));
    }
}
