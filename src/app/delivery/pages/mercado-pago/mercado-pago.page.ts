import { LoadingController, NavController, AlertController, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Checkout } from 'src/app/shared/models/checkout';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';
import { ModalPage } from 'src/app/shared/components/modal/modal.component';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../store';
import { setCurrentCard } from '../../../auth/store/auth.actions';
import { selectCurrentCard } from 'src/app/auth/store';

//import { ConsoleReporter } from 'jasmine';

declare var Mercadopago: any;

// 5175622852301034
// 123
// 11
// 25
// APRO
// 12345678

@Component({
  selector: 'app-mercado-pago',
  templateUrl: './mercado-pago.page.html',
  styleUrls: ['./mercado-pago.page.scss'],
})
export class MercadoPagoPage implements OnInit {
  checkoutDto: Checkout;
  deshabilitarBoton: boolean =false;
  publicKey:any;
  loading:any;
  currentCard: any;
  cartLength:number=0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private deliveryService: DeliveryService,
    private modalController: ModalController,
    private store: Store<State>
    )
  {
    this.activatedRoute.params.subscribe(data => {
      this.checkoutDto=JSON.parse(data.checkoutDto);

      this.deliveryService.getShopData(this.checkoutDto.shop_id).subscribe(data => {
        console.log(data);
        this.publicKey = data[0].mpPublicKey;
        //this.publicKey = data[0].mpProdPublicKey;
        //this.publicKey = 'TEST-1d931b6a-da4d-4f1f-ae56-ae57801c5055';
    
        Mercadopago.setPublishableKey(this.publicKey);
        console.log("PublicKey",this.publicKey);

      });

    });

    // Cargo valores de tarjeta
    this.store.select(selectCurrentCard)
    .subscribe((data: any) => {
      this.currentCard = data;
      console.log("MP constr: "+ JSON.stringify(data))
    });

  }

  ngOnInit() {

    console.log("MercadoPagoPage");

    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });

    Mercadopago.getIdentificationTypes();
    document.querySelector('ion-input[data-checkout="cardNumber"]').addEventListener('keyup', this.guessingPaymentMethod);
    document.querySelector('ion-input[data-checkout="cardNumber"]').addEventListener('change', this.guessingPaymentMethod);

    // Cargo el formulario con los valores leídos
    if(this.currentCard) {

      const cardNumber: any = document.querySelector('ion-input[data-checkout="cardNumber"]');
      const securityCode: any = document.querySelector('ion-input[data-checkout="securityCode"]');
      const cardExpirationMonth: any = document.querySelector('ion-input[data-checkout="cardExpirationMonth"]');
      const cardExpirationYear: any = document.querySelector('ion-input[data-checkout="cardExpirationYear"]');
      const cardholderName: any = document.querySelector('ion-input[data-checkout="cardholderName"]');
      const docType: any = document.querySelector('select[data-checkout="docType"]');
      const docNumber: any = document.querySelector('ion-input[data-checkout="docNumber"]');
      
      cardNumber.value = this.currentCard.cardNumber;
      securityCode.value = this.currentCard.securityCode;
      cardExpirationMonth.value = this.currentCard.cardExpirationMonth;
      cardExpirationYear.value = this.currentCard.cardExpirationYear;
      cardholderName.value = this.currentCard.cardholderName;
      docType.value = this.currentCard.docType;
      docNumber.value = this.currentCard.docNumber;

    }

  }
  
  
  checkCard()  {

    
    this.deshabilitarBoton = true;

    //this.presentLoading("Chequeando valores...");

    //this.checkoutDto.payment_token = "";

    // Chequeo que no hayan cambiado los precios
    // if (carri.calculateCart() !== Carrito.orderinfo.subtotal) {
    //   // Cambiaron algunos precios

    //   let alerta2 = alertCtrl.create({
      //     title: '¡Atención!',
    //     message: 'Algunos precios han cambiado, deberá revisar su pedido y dar su conformidad.',
    //     buttons: [
      //       {
        //         text: 'Aceptar',
    //         handler: data => {
    //           // Vuelvo a Home
    //           let navC = navControl.setRoot(HomePage);
    //         }
    //       }
    //     ]
    //   });
    //   alerta2.present();
    //   return;
    
    // }
    
    
    
    
    
    
    //console.log(securityCode.value);
    const $form = document.querySelector('#pay');
    Mercadopago.createToken($form, this.sdkResponseHandler); // The  function "sdkResponseHandler" is defined below
    // Mercadopago.createToken({
    //   "cardNumber" : '5175622852301034',
    //   "securityCode" : '123',
    //   "cardExpirationMonth" : '11',
    //   "cardExpirationYear" : '25',
    //   "cardholderName" : 'APRO',
    //   "docType" : 'DNI',
    //   "docNumber" : '12345678',
    //   "installments": 1
    //   }, sdkResponseHandler); // The  function "sdkResponseHandler" is defined below
    
  }

  sdkResponseHandler = (status, response) =>  {

    console.log("Response: "+ JSON.stringify(response));

    if (status != 200 && status != 201) {
      Mercadopago.clearSession();

      let erroresMP: Array<any> = [
        { code: "205", mensaje: "Ingresa el número de tu tarjeta." },
        { code: "208", mensaje: "Elige un mes." },
        { code: "209", mensaje: "Elige un año." },
        { code: "212", mensaje: "Ingresa tu tipo de documento." },
        { code: "212", mensaje: "Ingresa tu tipo de documento." },
        { code: "213", mensaje: "Ingresa tu documento." },
        { code: "214", mensaje: "Ingresa tu documento." },
        { code: "220", mensaje: "Ingresa tu banco." },
        { code: "221", mensaje: "Ingresa el nombre y apellido." },
        { code: "224", mensaje: "Ingresa el código de seguridad." },
        { code: "E301", mensaje: "Ingresa un número de tarjeta válido." },
        { code: "E302", mensaje: "Revisa el código de seguridad." },
        { code: "316", description: "Ingresa un nombre válido." },
        { code: "322", description: "El tipo de documento es inválido." },
        { code: "323", description: "Revisa tu documento." },
        { code: "324", mensaje: "El documento es inválido." },
        { code: "325", mensaje: "El mes es inválido." },
        { code: "326", mensaje: "El año es inválido." },
        { code: "default", mensaje: "Revisa los datos." },
      ];
      
      var mensajeErrorMP: any;
      var mensajeAlerta: string = "";
      for (var i = 0; i < response.cause.length; i++) {

        mensajeErrorMP = erroresMP.find(x => x.code == response.cause[i].code);
        if (mensajeErrorMP != null) {
          mensajeAlerta = mensajeAlerta + "<br>" + mensajeErrorMP.mensaje;
        } else {
          mensajeAlerta = mensajeAlerta + "<br>" + "Revisa los datos.";
        }

      }
      this.errorAlert("Error al validar", mensajeAlerta);
  

    } else {
      console.log("bien");

      let payment_id = '';
      var paymentMethodId = document.getElementsByName("paymentMethodId");
      if (paymentMethodId.length > 0) {
        let payment: any = paymentMethodId[0];
        payment_id = payment.value;
      }else {
        payment_id = this.currentCard.payment_id;
      }

      this.fillCurrentCard(response.id, payment_id);
      Mercadopago.clearSession();

      //this.loading.dismiss();
      this.navCtrl.back();
      return;

      //this.checkoutDto.payment_token = response.id;

     
      
      //this.checkout();
     
    }
  }
  
  checkout() {
    
    // // this.deliveryService.postCheckout(this.checkoutDto).pipe(
    // //   catchError(error => {
    // //     Mercadopago.clearSession();
    // //     this.deshabilitarBoton = false;
    // //     this.loading.dismiss();
    // //     console.log("Error MP: "+JSON.stringify(error));
    // //     return (error);
    // //   }),
    // // ).subscribe(data => {
    // //   Mercadopago.clearSession();
    // //   this.deshabilitarBoton = false;
    // //   //console.log("Respuesta: " + JSON.stringify(data));
    // //   if(data.order.payment_status == 'Pagado') {
    // //     this.store.dispatch(clearDelivery());
    // //     this.presentModal(data.order.id);
    // //   } else {
    // //     this.errorAlert("Se ha rechazado el pago", "Información adicional:<br><br>" + data.order.payment_status);
    // //   }
    // //   this.loading.dismiss();
    // // });
  }

  
  fillCurrentCard(token, id) {

    const cardNumber: any = document.querySelector('ion-input[data-checkout="cardNumber"]');
    const securityCode: any = document.querySelector('ion-input[data-checkout="securityCode"]');
    const cardExpirationMonth: any = document.querySelector('ion-input[data-checkout="cardExpirationMonth"]');
    const cardExpirationYear: any = document.querySelector('ion-input[data-checkout="cardExpirationYear"]');
    const cardholderName: any = document.querySelector('ion-input[data-checkout="cardholderName"]');
    const docType: any = document.querySelector('select[data-checkout="docType"]');
    const docNumber: any = document.querySelector('ion-input[data-checkout="docNumber"]');

    


    this.store.dispatch(setCurrentCard({ currentCard:
      { 
        cardNumber: cardNumber.value? cardNumber.value: '' ,
        securityCode: securityCode.value? securityCode.value: '',
        cardExpirationMonth: cardExpirationMonth.value? cardExpirationMonth.value: '',
        cardExpirationYear: cardExpirationYear.value? cardExpirationYear.value: '',
        cardholderName: cardholderName.value? cardholderName.value: '',
        docType: docType.value? docType.value: '',
        docNumber: docNumber.value? docNumber.value: '',
        payment_token: token,
        payment_id: id
      }

    }));

  }

  

  guessingPaymentMethod(event) {
    const bin = getBin();

    if (event.type == 'keyup') {
      if (bin.length >= 6) {
        Mercadopago.getPaymentMethod({
          'bin': bin
        }, setPaymentMethodInfo);
      }
    } else {
      setTimeout(function () {
        if (bin.length >= 6) {
          Mercadopago.getPaymentMethod({
            'bin': bin
          },
            setPaymentMethodInfo);
        }
      }, 100);
    }

    function getBin() {
      const ccNumber: any = document.querySelector(
        'ion-input[data-checkout="cardNumber"]'
      );
      return ccNumber.value.replace(/[ .-]/g, '').slice(0, 6);
    }

    function setPaymentMethodInfo(status, response) {
      if (status == 200) {
        // do somethings ex: show logo of the payment method
        const form = document.querySelector('#pay');

        if (document.querySelector('ion-input[name=paymentMethodId]') == null) {
          const paymentMethod = document.createElement('ion-input');
          paymentMethod.setAttribute('name', 'paymentMethodId');
          paymentMethod.setAttribute('type', 'hidden');
          paymentMethod.setAttribute('value', response[0].id);

          form.appendChild(paymentMethod);
        } else {
          const tag: any = document.querySelector('ion-input[name=paymentMethodId]');
          tag.value = response[0].id;
        }
      }
    }
  }

  //'Borrowed' from //https://angular.io/docs/ts/latest/guide/server-communication.html
  // private extractData(res: Response) {
  //   //Convert the response to JSON format
  //   let body = res.json();
  //   //Return the data (or nothing)
  //   return body || {};
  // }

  //'Borrowed' from //https://angular.io/docs/ts/latest/guide/server-communication.html
  // private handleError(res: Response | any) {
  //   return Promise.reject(res.message || res);
  // }

  private async errorAlert(header, message) {

    
    let alert = await this.alertCtrl.create({
        cssClass: 'alert-error',
        backdropDismiss: false,
        header: header,
        mode: 'ios',
        message: message,
        buttons: [{
            text: 'Aceptar',
            handler: data => {
              this.deshabilitarBoton = false;
            }
        }]
    });
    // Dismiss previously created loading
    if (this.loading != null) {
      this.loading.dismiss();
    }
    await alert.present();
  }

  async presentLoading(message) {
    this.loading = await this.loadingCtrl.create({
      message: message,
      spinner: 'crescent'
    });
    await this.loading.present();
  }

  async presentModal(orderId) {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        type: 2, // type 2 = delivery checkout
        orderId
      }
    });
    modal.onDidDismiss().then(() => {
    });
    return await modal.present();
  }
  


}
