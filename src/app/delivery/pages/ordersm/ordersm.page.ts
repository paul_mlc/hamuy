import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { STRINGS } from 'src/app/shared/constants/delivery.constants';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-ordersm',
  templateUrl: './ordersm.page.html',
  styleUrls: ['./ordersm.page.scss'],
})
export class OrdersmPage implements OnInit {
  orders$: Observable<any>;
  baseUrl = environment.image_base_url;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    console.log('OrdersmPage');
    this.orders$ = this.activatedRoute.data.pipe(
      map(data => {
        console.log("data: ", data);
        if (data.ordersm.orders.length) {
          return data.ordersm.orders.map(order => {
            const o = order;
            o.shop = JSON.parse(order.shop);
            o.address = JSON.parse(order.address);
            o.progress = STRINGS.progress[o.state];
            o.time = order.order_datetime.substr(11, 2) + ':' + order.order_datetime.substr(14, 2);
            if (data.ordersm.pendientes.length > 0){
              o.pendientes = 1;
            } else {
              o.pendientes = 0;
            }
            
            // console.log("o: ",o);
            return o;
           
          });
        }
      })
    );
    
    
  }

  ngOnInit() {
  }

  goToTakeOrder(order) {
    console.log("order taken: ",order)
    //order.delivery_address=order.address.address;
    //order.shop_address=order.shop.address;
    this.router.navigate(['delivery/ordertake', { order: JSON.stringify(order) }]);
  }
}