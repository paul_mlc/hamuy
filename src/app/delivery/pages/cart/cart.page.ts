import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength, selectGetCart } from '../../store';
import { removeProductFromCart, updateProductFromCart } from '../../store/delivery.actions';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  items = [];
  total = 0;
  shopId;
  baseUrl = environment.image_base_url;
  cartLength:number=0;

  constructor(
    private router: Router,
    private store: Store<State>,
    private activatedRoute: ActivatedRoute
  ) {
    console.log('CartPage');
    this.activatedRoute.params.subscribe(data => {
      this.shopId = data.shopId;
    });
  }

  ngOnInit() {
    this.store.select(selectGetCart).subscribe(cart => {
      this.items = cart;
      this.calculateTotal();
    });
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
  }

  openCheckout() {
    //this.router.navigate(['delivery/checkout', { shopId: Number(this.shopId) }]);
    this.router.navigate(['delivery/checkout']);
  }

  calculateTotal() {
    this.total = 0;
    this.items.map((p: any) => {
      this.total = (p.product.price_with_percentage * p.quantity) + this.total;
    });
  }

  udpateQuantity(item) {
    item.quantity === 0
      ? this.store.dispatch(removeProductFromCart({ item }))
      : this.store.dispatch(updateProductFromCart({ item} ));
    this.calculateTotal();
  }

  remove(item) {
    this.store.dispatch(removeProductFromCart({ item }));
  }

  less(product) {
    const item = { ...product };
    if (item.quantity === 1) { return false; }
    item.quantity -= 1;
    this.udpateQuantity(item);
  }

  more(product) {
    const item = { ...product };
    if (item.quantity === 10) { return false; }
    item.quantity += 1;
    this.udpateQuantity(item);
  }
}
