import { Component, OnInit } from '@angular/core';
import { combineLatest } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../store';
import { DeliveryService } from '../../services/delivery.service';
import { exhaustMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-shops',
  templateUrl: './shops.page.html',
  styleUrls: ['./shops.page.scss'],
})
export class ShopsPage implements OnInit {
  shops = [];
  filteredShops = [];
  title: any = 'RESTAURANTES';
  baseUrl = environment.image_base_url;
  cartLength:number=0;
  
  constructor(
    private deliveryService: DeliveryService,
    private store: Store<State>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    console.log('ShopsPage');
  }

  ngOnInit() {
    combineLatest([
      this.activatedRoute.params,
      // this.store.select(selectCurrentAddress),
    ])
      .pipe(
        exhaustMap(data => {
          this.title = data[0].categoryName;
          // return this.deliveryService.getShops(data[0].subcategoryId, data[1].id);
          return this.deliveryService.getShopsMock(data[0].subcategoryId);
        }
        )
      )
      .subscribe(data => {
        this.shops = data;
        this.filteredShops = data;
        //this.store.dispatch(clearDelivery());
      });
  }

  ionViewDidEnter() {
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
  }

  openShop(shop) {
    this.router.navigate(['delivery/products',
      { shopId: shop.id, name: shop.name, description: shop.description }]);
  }

  filterItems($event) {
    if ($event.target.value.length < 3) {
      this.filteredShops = this.shops;
      return false;
    }
    this.filteredShops = this.shops.filter(s => {
      return (s.name.toLowerCase()).includes($event.target.value.toLowerCase());
    });
  }
}
