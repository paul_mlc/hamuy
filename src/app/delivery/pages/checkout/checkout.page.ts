import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, AlertController, NavController } from '@ionic/angular';
import { ModalPage } from 'src/app/shared/components/modal/modal.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Subject, BehaviorSubject, forkJoin, combineLatest, of } from 'rxjs';
import { catchError, map, filter, switchMap, concatMap, exhaustMap, mergeAll } from 'rxjs/operators';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { Checkout } from 'src/app/shared/models/checkout';
import { STRINGS } from 'src/app/shared/constants/delivery.constants';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../store';
import { selectCurrentAddress, selectCurrentCard, selectPaymentMethdos } from 'src/app/auth/store';
import { initCurrentCard } from '../../../auth/store/auth.actions';
import { selectGetCart } from '../../store';
import { shopId } from '../../store';
import { clearDelivery } from '../../store/delivery.actions';
import { OrderCompleteComponent } from '../../components/order-complete/order-complete.component';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {
  shopId: number;
  items = [];
  products = [];
  totalProducts = 0;
  deliveryFee = 0;
  total = 0;
  eta = 0;
  address;
  $total: Subject<any> = new Subject<any>();
  checkoutDto: Checkout;
  paymentMethod;
  loading;
  checkoutButtonDisabled = true;
  currentCard: any = null;
  textoBoton:string = '';
  cartLength:number=0;

  constructor(
    private router: Router,
    private modalController: ModalController,
    private alertCtrl: AlertController,
    private storage: Storage,
    private navCtrl: NavController,
    private storageService: StorageService,
    private deliveryService: DeliveryService,
    private loadingController: LoadingController,
    private activatedRoute: ActivatedRoute,
    private store: Store<State>
  ) {
    console.log('CheckoutPage');
  }

  ngOnInit() {
    // this.cardData = { "cardNumber" : '5175622852301034',
    //    "securityCode" : '123',
    //    "cardExpirationMonth" : '11',
    //    "cardExpirationYear" : '25',
    //    "cardholderName" : 'APRO',
    //    "docType" : 'DNI',
    //    "docNumber" : '12345678',
    //    "installments": 1
    // }

    combineLatest([
      this.store.select(selectCurrentAddress),
      this.store.select(selectPaymentMethdos),
      this.store.select(selectGetCart),
      this.store.select(shopId),
      this.store.select(selectCurrentCard),
      this.store.select(cartLength),
      // this.activatedRoute.params,
    ])
      .pipe(
        map(data => {
          this.address = data[0];
          this.paymentMethod = data[1][0];
          this.items = data[2];
          this.shopId = +data[3];
          this.cartLength = +data[5];

          return data;
        }),
        exhaustMap(data =>
          forkJoin([
            this.deliveryService.getEstimatedTime({
              shopId: +data[3],
              addressId: data[0].id
            }),
            this.deliveryService.getDeliveryFee({
              shopId: +data[3],
              addressId: data[0].id
            })
          ])
        ),
        catchError(error => {
          this.checkoutButtonDisabled = true;
          this.navCtrl.back();
          return of(error);
        })
      )
      .subscribe(data => {
        if (data instanceof Array) {
          this.eta = data[0].data;
          this.deliveryFee = data[1].data;
          this.$total.next();
          this.calculateTotalProducts();
          this.checkoutDto = {
            address_id: this.address.id,
            shop_id: this.shopId,
            products: this.products,
            user_payment_method_id: this.paymentMethod.id,
            payment_token: '',
            payment_id: '',
          };
          console.log(this.checkoutDto);

          this.checkoutButtonDisabled = false;
        }
      });

    this.$total.asObservable().subscribe(() => {
      this.total = this.totalProducts + this.deliveryFee;
    });


  }

  ionViewDidEnter() {

    // Cargo valores de tarjeta
    this.store.select(selectCurrentCard)
    .subscribe((data: any) => {
      this.currentCard = data;
      console.log("MP constr: "+ JSON.stringify(data))
    });

    // Texto del Botón
    if (this.currentCard == null ) {
      this.textoBoton = 'Ingresar tarjeta';
    } else if(this.currentCard.payment_token == ''){
      this.textoBoton = 'Validar tarjeta';
    } else {
      this.textoBoton = 'Realizar pago';
    }
    
  }

  
  checkout() {
    

    if (this.currentCard == null ) {
      this.errorAlert("Ingrese tarjeta","A continuación le pediremos los datos de la tarjeta.");
      this.cardDataFill();
    } else if(this.currentCard.payment_token == ''){
      this.errorAlert("Falta validar la tarjeta","Revise los datos de la tarjeta y realice la validación.");
      this.cardDataFill();
    } else {
      this.presentLoading();
      this.checkoutDto.payment_token = this.currentCard.payment_token;
      this.checkoutDto.payment_id = this.currentCard.payment_id;
      this.deliveryService.postCheckout(this.checkoutDto).pipe(
        catchError(error => {
          this.loading.dismiss();
          this.store.dispatch(initCurrentCard(''));
          console.log("Error MP: "+JSON.stringify(error));
          return (error);
        }),
      ).subscribe(data => {
        if(data.order.payment_status == 'Pagado') {
          this.store.dispatch(clearDelivery());
          this.store.dispatch(initCurrentCard(''));
          this.errorAlert('¡Felicitaciones!','La orden nro. '+data.order.id + ' ha sido creada.' )
          //this.presentModal(data.order.id);
          this.navCtrl.navigateRoot(['delivery/order', { orderId: data.order.id }]);
        } else {
          this.store.dispatch(initCurrentCard(''));
          this.errorAlert("Se ha rechazado el pago", "Información adicional:<br><br>" + data.order.payment_status);
        }
        this.loading.dismiss();
      });
    }
    
  }

  async presentModal(orderId) {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        type: 2, // type 2 = delivery checkout
        orderId
      }
    });
    modal.onDidDismiss().then(() => {
    });
    return await modal.present();
  }

  private async errorAlert(header, message) {
    
    let alert = await this.alertCtrl.create({
        cssClass: 'alert-error',
        backdropDismiss: false,
        header: header,
        mode: 'ios',
        message: message,
        buttons: [{
            text: 'Aceptar',
            handler: data => {
              this.checkoutButtonDisabled = false;
            }
        }]
    });
    // Dismiss previously created loading
    if (this.loading != null) {
      this.loading.dismiss();
    }
    await alert.present();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: STRINGS.order.generating,
    });
    await this.loading.present();
  }

  calculateTotalProducts() {
    this.totalProducts = 0;
    this.items.map((p: any) => {
      this.totalProducts = (p.product.price_with_percentage * p.quantity) + this.totalProducts;
      this.products.push({ id: p.product.id, quantity: p.quantity });
    });
    this.$total.next();
  }

  cardDataFill() {
    this.router.navigate(['delivery/mercadopago', { checkoutDto: JSON.stringify(this.checkoutDto) } ]);
  }

  async modalOrderComplete() {
    const modal = await this.modalController.create({
      component: OrderCompleteComponent, componentProps: {}, cssClass: 'ordercomplete-modal',
    });

    return await modal.present();
  }

}

