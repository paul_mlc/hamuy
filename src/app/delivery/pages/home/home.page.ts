import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, AlertController, Platform } from '@ionic/angular';
import { SupportComponent } from 'src/app/shared/components/support/support.component';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../store';
import { selectCurrentAddress, selectModalidad } from 'src/app/auth/store';
import { filter } from 'rxjs/operators';
import { initUserData } from 'src/app/auth/store/auth.actions';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { NewAddressPage } from 'src/app/shared/components/new-address/new-address.component';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  hasCurrentAddress = false;
  address: any;
  modalidad: number;
  cartLength: number = 0;
  nombre: string = "";
  ingresosTotales: any;

  constructor(
    private router: Router,
    private modalController: ModalController,
    private alertController: AlertController,
    private store: Store<State>,
    public oneSignal: OneSignal,
    public platform: Platform,
    private deliveryService: DeliveryService
  ) {
    console.log('Home constructor');
  }

  ionViewDidEnter() {
    console.log('Home Did Enter');
    console.log('localStorage', localStorage);
    // let temp = JSON.parse(localStorage.getItem('hamuyapp_auth'));
    // this.modalidad = temp.modalidad;
    this.store.dispatch(initUserData());
    this.hasCurrentAddress = false;
    this.store.pipe(select(selectCurrentAddress), filter(a => !!a))
      .subscribe(address => {
        this.hasCurrentAddress = true;
        this.address = address.address.split(',')[0];
      });

    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
    this.store.select(selectModalidad).subscribe(modalidad => {
      this.modalidad = modalidad;
    });
    
    if (this.modalidad == 2) {  
      this.deliveryService.ingresosMotorizado()
        .subscribe((res) => {
          console.log('Ingresos',res);
          this.ingresosTotales = res.ingresos.commission;
        });


    }

  }

  ngOnInit() {
    console.log('Home Init');
    let hamuyapp_auth = JSON.parse(localStorage.getItem('hamuyapp_auth'));
    this.nombre = hamuyapp_auth.user.name;
    if (this.platform.is('cordova')) {

      if (hamuyapp_auth.user.id) {

        console.log('nombre', this.nombre);
        console.log('User id', hamuyapp_auth.user.id);
        console.log('Modalidadd', hamuyapp_auth.modalidad);
        this.oneSignal.sendTags({
          idUsuario: hamuyapp_auth.user.id,
          modalidad: hamuyapp_auth.modalidad

        });
      }

      console.log('LocalStorage app component', localStorage);

    }

  }

  goToShops() {
    if (!this.hasCurrentAddress) {
      this.presentAlert();
      return false;
    }
    this.router.navigate(['delivery/shops']);
  }

  goToSubcategories(categoryId, categoryName) {
    if (!this.hasCurrentAddress) {
      this.presentAlert();
      return false;
    }
    this.router.navigate(['delivery/subcategories', { categoryId, categoryName }]);
  }

  goToUser() {
    this.router.navigate(['auth/user']);
  }

  goToSearch() {
    if (!this.hasCurrentAddress) {
      this.presentAlert();
      return false;
    }
    this.router.navigate(['delivery/search']);
  }

  async openNewAddressModal() {
    const modal = await this.modalController.create({
      component: NewAddressPage,
    });
    modal.onDidDismiss().then(() => {
    });
    return await modal.present();
  }

  async modalSupport() {
    const modal = await this.modalController.create({
      component: SupportComponent, componentProps: {}, cssClass: 'support-modal',
    });

    return await modal.present();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Necesitas una ubicación',
      mode: 'ios',
      message: 'Empecemos eligiendo una ubicación/dirección',
      buttons: [
        {
          text: '¡Vamos!',
          handler: () => this.router.navigate(['auth/user'])
        }
      ]
    });

    await alert.present();
  }
}
