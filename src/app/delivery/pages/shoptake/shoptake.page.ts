import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { STRINGS } from 'src/app/shared/constants/delivery.constants';
import { environment } from 'src/environments/environment';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';

@Component({
  selector: 'app-shoptake',
  templateUrl: './shoptake.page.html',
  styleUrls: ['./shoptake.page.scss'],
})
export class ShoptakePage implements OnInit {

  order:any;
  orders$: Observable<any>;
  baseUrl = environment.image_base_url;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private deliveryService: DeliveryService
  ) {
    console.log('ShoptakePage');
    this.activatedRoute.params.subscribe(data => {
      console.log(data);
      this.order = data;
    });
  
  }

  ngOnInit() {
  }

  takeOrder(order) {

    this.deliveryService.shopTake(order)
      .subscribe((res) => {
        console.log("hola");
        console.log(res);
        this.router.navigate(['delivery/shoptaken']);
      });
  }
}
