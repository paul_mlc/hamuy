import { Component, OnInit } from '@angular/core';
import { AddressComponent } from '../../components/address/address.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-courier',
  templateUrl: './courier.page.html',
  styleUrls: ['./courier.page.scss'],
})
export class CourierPage implements OnInit {

  constructor(private modalController: ModalController) {
    console.log('CourierPage');
   }

  ngOnInit() {
  }

  async openNewAddressModal() {
    const modal = await this.modalController.create({
      component: AddressComponent,
    });
    modal.onDidDismiss().then(data => {
      console.log(data);
    });
    return await modal.present();
  }


}
