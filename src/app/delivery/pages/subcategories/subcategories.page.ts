import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../store';

@Component({
  selector: 'app-subcategories',
  templateUrl: './subcategories.page.html',
  styleUrls: ['./subcategories.page.scss'],
})
export class SubcategoriesPage implements OnInit {

  subcategories = [];
  filteredSubcategories = [];
  title = 'Restaurantes';
  baseUrl = environment.image_base_url;
  cartLength:number=0;
  // const removeAccents = (str) => {
  //   return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  // }
  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<State>,
    private router: Router,
  ) {
    console.log('SubcategoriesPage');
    this.activatedRoute.params.subscribe(data => {
      this.title = data.categoryName;
    });
    this.activatedRoute.data.pipe(
      map(data => data.subcategories)
    ).subscribe(data => {
      this.subcategories = data;
      this.filteredSubcategories = data;
    });
  }

  ngOnInit() {
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
  }

  openShops(subcategoryId) {
    this.router.navigate(['delivery/shops', { subcategoryId }]);
  }

  filterItems($event) {
    if ($event.target.value.length < 3) {
      this.filteredSubcategories = this.subcategories;
      return false;
    }
    this.filteredSubcategories = this.subcategories.filter(s => {
      return (this.removeAccents(s.name.toLowerCase())).includes(this.removeAccents($event.target.value.toLowerCase()));
    });
  }

  removeAccents(str) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }

}
