import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SubcategoriesResolverService implements Resolve<any> {
    constructor(private deliveryService: DeliveryService) { }

    resolve(route: ActivatedRouteSnapshot) {
        const categoryId = route.paramMap.get('categoryId');
        return this.deliveryService.getSubCategories(categoryId)
            .pipe(catchError(error => {
                return of(error);
            }));
    }
}
