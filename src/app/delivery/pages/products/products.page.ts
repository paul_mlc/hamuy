import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';

import { DeliveryService } from 'src/app/delivery/services/delivery.service';
import { Store, select  } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../store';
import { addFavourite } from 'src/app/auth/store/auth.actions';
import { selectFavourites } from 'src/app/auth/store';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  products: any = [];
  filteredProducts: any = [];
  shopName = '';
  description = '';
  categoryButtons = [];
  categories = [];
  shopId;
  cartLength:number=0;
  isFavourite = false;
  baseUrl = environment.image_base_url;
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public actionSheetController: ActionSheetController,
    private deliveryService: DeliveryService,
    private store: Store<State>
  ) {
    console.log('ProductsPage');
    this.activatedRoute.params.subscribe(data => {
      this.shopName = data.name;
      this.shopId = data.shopId;
      this.description = data.description;

      this.store.select(selectFavourites).subscribe(favourites => {
        this.isFavourite = !!favourites.find(f => +f.shop.id === +this.shopId);
      });

      this.deliveryService.getProductCategoriesByShop(data.shopId).subscribe(categories => {
        this.categoryButtons = categories.map(c => {
          return {
            text: c.name,
            role: 'destructive',
            handler: this.filterProducts.bind(this, c.id)
          };
        });
        this.categoryButtons.unshift({
          text: 'Todos',
          role: 'destructive',
          handler: () => this.filteredProducts = this.products
        });
      });
    });
    this.activatedRoute.data.subscribe(data => {
      this.products = data.products;
      this.filteredProducts = data.products;
    });

  }

  ngOnInit() {
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
  }

  openCart() {
    this.router.navigate(['delivery/cart', { shopId: this.shopId }]);
  }

  async presentCategoriesActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Categorías',
      buttons: this.categoryButtons
    });
    await actionSheet.present();
  }

  async presentFilterActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Filtrar por',
      buttons: [{
        text: 'Mayor precio',
        role: 'destructive',
        handler: this.orderProducts.bind(this, false)
      }, {
        text: 'Menor precio',
        role: 'destructive',
        handler: this.orderProducts.bind(this)
      }]
    });
    await actionSheet.present();
  }

  filterProducts(categoryId) {
    this.filteredProducts = this.products.filter(p => +p.product_category_id === +categoryId);
  }

  orderProducts(asc = true) {
    this.filteredProducts = this.filteredProducts.sort((a, b) => {
      return asc ? a.price_with_percentage - b.price_with_percentage : b.price_with_percentage - a.price_with_percentage;
    });
  }

  addProductToCart(product) {
    product.shopId = this.shopId;
    this.router.navigate(['delivery/product', { ...product }]);
  }

  filterItems($event) {
    if ($event.target.value.length < 3) {
      this.filteredProducts = this.products;
      return false;
    }
    this.filteredProducts = this.products.filter(s => {
      return (this.removeAccents(s.name.toLowerCase())).includes(this.removeAccents($event.target.value.toLowerCase()));
    });
  }

  removeAccents(str) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }

  addFavourite() {
    this.store.dispatch(addFavourite({ shop_id: this.shopId }));
  }
}
