import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';

@Injectable({
    providedIn: 'root'
})
export class ProductsResolverService implements Resolve<any> {
    constructor(private deliveryService: DeliveryService) { }

    resolve(route: ActivatedRouteSnapshot) {
        const shopId = route.paramMap.get('shopId');
        return this.deliveryService.getProductsByShop(shopId);
    }
}
