import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../store';
import { clearDelivery, addProductToCart } from '../../store/delivery.actions';
import { shopId } from '../../store';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  product;
  lastShopId;
  currentShopId;
  quantity = 1;
  baseUrl = environment.image_base_url;
  cartLength:number=0;

  constructor(
    private navCtrl: NavController,
    private alertController: AlertController,
    private activatedRoute: ActivatedRoute,
    private store: Store<State>
  ) {
    console.log('ProductPage');
    this.activatedRoute.params.subscribe(data => {
      console.log(data);
      this.product = data;
      this.currentShopId = data.shopId;
    });
    this.store.select(shopId).subscribe(shopId => this.lastShopId = shopId);
  }

  ngOnInit() {
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
  }

  less() {
    if (this.quantity === 1) { return false; }
    this.quantity -= 1;
  }

  more() {
    if (this.quantity === 10) { return false; }
    this.quantity += 1;
  }

  addAndReturnToProducts() {
  }

  addProduct() {
    // Obtengo el shopId para ver si tengo que vaciar el carrito
    // this.store.select(shopId).subscribe(shopId => {
    //   this.currentShopId = shopId;
      if(this.lastShopId !== "" && this.lastShopId !== this.currentShopId ) {
        this.decisionAlert();
      } else {
        this.addProductToCart();  
      }
    // });
  }

  addProductToCart() {
    this.store.dispatch(addProductToCart({ shopId: this.currentShopId, product: this.product, quantity: this.quantity }));
    this.navCtrl.back();
  }

  async decisionAlert() {
    const alert = await this.alertController.create({
        cssClass: 'alert-error',
        header: '¡Atención!',
        mode: 'ios',
        message: `<p>Ya hay cargado algún producto de otro restaurante.</p><p>¿Desea agregar este producto vaciando previamente el carrito?</p>`,
        buttons: [{
            text: 'Aceptar',
            handler: () => {
              this.store.dispatch(clearDelivery());
              this.addProductToCart();
            }
            },
            {
              text: 'Cancelar',
              handler: () => {}
            }
        ]
    });

    await alert.present();
  }
}
