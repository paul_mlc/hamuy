import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../services/search.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../store';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  products = [];
  shops = [];
  baseUrl = environment.image_base_url;
  loading = false;
  cartLength:number=0;

  constructor(
    private searchService: SearchService,
    private store: Store<State>,
    private router: Router
  ) {
    console.log('SearchPage');
  }

  ngOnInit() {
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
  }

  getAutocompleteList($event) {
    if ($event.target.value.length < 3) {
      return false;
    }
    this.loading = true;
    this.searchService.globalSearch({ q: $event.target.value })
      .subscribe(res => {
        this.products = res.products;
        console.log(this.products);
        this.shops = res.shops;
        this.loading = false;
      });
  }

  openShop(shop) {
    // if (shop.opened === true) {
    this.router.navigate(['delivery/products',
      { shopId: shop.id, name: shop.name, description: shop.description }]);
    // }
  }

}
