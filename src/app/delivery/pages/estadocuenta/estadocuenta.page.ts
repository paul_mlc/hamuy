import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { STRINGS } from 'src/app/shared/constants/delivery.constants';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-estadocuenta',
  templateUrl: './estadocuenta.page.html',
  styleUrls: ['./estadocuenta.page.scss'],
})
export class EstadocuentaPage implements OnInit {
  weeks$: Observable<any>;
  baseUrl = environment.image_base_url;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router)
  {
    console.log('EstadocuentaPage');

    
       
    this.weeks$ = this.activatedRoute.data.pipe(
      map(data => {
        console.log("data: ", data);
        if (data.estadocuenta.length) {
          return data.estadocuenta.map(week => {
            const w = week;
            // o.shop = JSON.parse(order.shop);
            // o.address = JSON.parse(order.address);
            // o.progress = STRINGS.progress[o.state];
            // console.log("o: ",o);
            return w;
           
          });
        }
      })
    );
    
    
  }

  ngOnInit() {
  }

}