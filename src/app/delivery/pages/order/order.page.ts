import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../store';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {
  employee: any;
  order: any;
  shop: any;
  address: any;
  products: any;
  baseUrl = environment.image_base_url;
  monto:number=0;
  cartLength:number=0;

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store<State>) {
    console.log('OrderPage');
  }

  ngOnInit() {
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
  }

  ionViewDidEnter() {
    this.activatedRoute.data.subscribe(data => {
      console.log('Order: ', data.order);
      this.employee = data.order.employee;
      this.order = data.order.order;
      this.products = JSON.parse(data.order.order.products);
      this.monto = Number(this.order.product_cost) + Number(this.order.cost_shipping);
      switch(this.order.state) { 
        case 'created': { 
          this.order.state='EN CURSO'; 
          break; 
       } 
        case 'accepted': { 
           this.order.state='ACEPTADO'; 
           break; 
        } 
        case 'taken': { 
          this.order.state='TOMADO'; 
          break; 
        } 
        case 'hold': { 
          this.order.state='HOLD'; 
          break; 
        } 
        case 'way': { 
        this.order.state='EN CAMINO'; 
        break; 
        }
        case 'arrived': { 
          this.order.state='ARRIBADO'; 
          break; 
        }
        case 'delivered': { 
          this.order.state='ENTREGADO'; 
          break; 
        }
        case 'cancelled': { 
          this.order.state='CANCELADO'; 
          break; 
        }
        case 'rejected': { 
          this.order.state='RECHAZADO'; 
          break; 
        }
      }
      // this.order.shop = JSON.parse(this.order.shop);
      // console.log('hola 2');
      // this.logo = JSON.parse(this.order.shop);
      // console.log('hola 3 ');
      // console.log('Logo: ', this.logo);
      this.address = JSON.parse(this.order.address);
      this.shop = JSON.parse(this.order.shop);
      console.log('Shop: ', this.shop);
    });
  }

  // setInterval(()=> {
  //   this.getData(); },4000); 
  // }

}
