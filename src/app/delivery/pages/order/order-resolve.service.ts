import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { DeliveryService } from 'src/app/delivery/services/delivery.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class OrderResolverService implements Resolve<any> {
    constructor(private deliveryService: DeliveryService) { }

    resolve(route: ActivatedRouteSnapshot) {
        const orderId = route.paramMap.get('orderId');
        return this.deliveryService.getOrder(+orderId).pipe(map(r => r.detail));
    }
}
