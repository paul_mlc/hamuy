import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
declare var google;

@Component({
  selector: 'app-mapashop',
  templateUrl: './mapashop.page.html',
  styleUrls: ['./mapashop.page.scss'],
})
export class MapashopPage implements OnInit {
  
  order:any;
  map;
  
  constructor(
    private geolocation: Geolocation,
    private alertController: AlertController,
    private activatedRoute: ActivatedRoute,
  ) {
      this.activatedRoute.params.subscribe(data => {
        this.order = JSON.parse(data.order);
      });
  }
    
  ngOnInit() {
  }

  ionViewDidEnter() {
    var currentLat: number;
    var currentLng: number;

    this.setMapConfiguration().then(() => {
      this.geolocation.getCurrentPosition().then(resp => {
        currentLat = +resp.coords.latitude;
        currentLng = +resp.coords.longitude;
        // currentLat = -12.069802;
        // currentLng = -75.214348;
        var centerLat = ( currentLat + Number(this.order.address.latitude)+ Number(this.order.shop.latitude)) / 3;
        var centerLng = ( currentLng + Number(this.order.address.longitude) + Number(this.order.shop.longitude)) / 3;
        this.map.setCenter(new google.maps.LatLng(centerLat, centerLng));
        this.map.setZoom(15);

        this.ubicacionMotorizado({lat: currentLat, lng: currentLng}, this.map);
        this.ubicacionShop({lat: +this.order.shop.latitude, lng: +this.order.shop.longitude}, this.map);
        this.ubicacionCliente({lat: +this.order.address.latitude, lng: +this.order.address.longitude}, this.map);

      }).catch(() => {
        this.alertOnGPSFails();
      });
    });
    
  }

  ubicacionMotorizado(position, map) {

    var marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position,
      icon: 'https://hamuyperu.com/googlemaps/blue-dot.png'
      // label: {text: "Estoy acá", color: "red"}
    });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
 
    var contentString = 
      '<h6 style="margin:0px; color: rgb(41, 105, 176);">Estoy acá</h6>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    infowindow.open(map, marker);

    return marker

  }

  ubicacionShop(position, map) {

    var marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position,
      icon: 'https://hamuyperu.com/googlemaps/yellow-dot.png'
    });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
 
    var contentString = 
      `<h6 style="margin:0px; color: rgb(41, 105, 176);">${this.order.shop.name}<br>${this.order.shop.address}</h6>`;

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    infowindow.open(map, marker);

    return marker

  }

  ubicacionCliente(position, map) {

    var marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position,
      icon: 'https://hamuyperu.com/googlemaps/gray-dot.png'
    });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
 
    var contentString = 
      `<h6 style="margin:0px; color: rgb(41, 105, 176);">Cliente<br>${this.order.address.address.split(',')[0]}</h6>`;

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    infowindow.open(map, marker);

    return marker

  }

  setMapConfiguration(): Promise<any> {
    return new Promise(resolve => {

      const mapEl: HTMLElement = document.getElementById('map');
      this.map = new google.maps.Map(mapEl, {
        disableDefaultUI: true,
        //center: new google.maps.LatLng(-12.067838, -75.210126),
        //zoom: 19,
        zoomControl: true,
        styles: [
          {
            featureType: 'poi',
            stylers: [
              { visibility: 'off' }
            ]
          }
        ]
      });
      
      resolve();
    });
  }

  async alertOnGPSFails() {
    const alert = await this.alertController.create({
      header: 'Oops',
      message: 'Parece que tu gps está desactivado o no le diste permisos a la aplicación.',
      mode: 'ios',
      buttons: ['OK']
    });

    await alert.present();
  }


}
