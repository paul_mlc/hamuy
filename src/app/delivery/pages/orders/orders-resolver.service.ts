import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from 'src/app/auth/services/user.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class OrdersResolverService implements Resolve<any> {
    constructor(private userService: UserService) { }

    resolve() {
        return this.userService.orders().pipe(map(r => r.orders));
    }
}
