import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { STRINGS } from 'src/app/shared/constants/delivery.constants';
import { environment } from 'src/environments/environment';
import { Store, select } from '@ngrx/store';
import { State } from 'src/app/app.reducer';
import { cartLength } from '../../store';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  orders$: Observable<any>;
  baseUrl = environment.image_base_url;
  estado:string = "";
  cartLength:number=0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<State>,
    private router: Router,
  ) {
    console.log('OrdersPage');
    this.orders$ = this.activatedRoute.data.pipe(
      map(data => {
        console.log('Data', data);
        if (data.orders.length) {
          return data.orders.map(order => {
            const o = order;
            o.shop = JSON.parse(order.shop);
            o.progress = STRINGS.progress[o.state];
            switch(o.state) { 
              case 'created': { 
                o.state='EN CURSO'; 
                break; 
             } 
              case 'accepted': { 
                 o.state='ACEPTADO'; 
                 break; 
              } 
              case 'taken': { 
                o.state='TOMADO'; 
                break; 
              } 
              case 'hold': { 
                o.state='HOLD'; 
                break; 
              } 
              case 'way': { 
              o.state='EN CAMINO'; 
              break; 
              }
              case 'arrived': { 
                o.state='ARRIBADO'; 
                break; 
              }
              case 'delivered': { 
                o.state='ENTREGADO'; 
                break; 
              }
              case 'cancelled': { 
                o.state='CANCELADO'; 
                break; 
              }
              case 'rejected': { 
                o.state='RECHAZADO'; 
                break; 
              }
            }
            return o;
          });
        }
      })
    );
  }

  ngOnInit() {
    this.store.select(cartLength).subscribe(length => {
      this.cartLength = length;
    });
  }

  goToOrder(orderId, status) {
    // if (status === 'delivered' || status === 'cancelled' || status === 'rejected') {
    //   // TODO: detalle
    // } else {
    this.router.navigate(['delivery/order', { orderId }]);
    // }
  }
}
